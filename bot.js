'use strict';
let driver = require('webdriverio');
let settings = require('./bot/settings');
let Router = require('./bot/page/index.js');
let client = driver.remote(settings);
client.init().then(function() {
    Router.bitbucket.login.doLogin(client);
});

# Load the extension #

Visit **[chrome://extensions](chrome://extensions)** in your browser (or open up the Chrome menu by clicking the icon to the far right of the Omnibox: The menu's icon is three horizontal bars.. and select *Extensions* under the *Tools* menu to get to the same place).

Ensure that the **Developer mode** checkbox in the top right-hand corner is checked.

Click **Load unpacked extension...** to pop up a file-selection dialog.

Navigate to the directory *scripts/chrome_extension* in the project, and select it.

Now you will have a new context menu in every environment you have Odigeo App deployed.

-- _alvaro.tanarro@odigeo.com_
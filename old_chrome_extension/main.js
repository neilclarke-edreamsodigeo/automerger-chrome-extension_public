/**
 * Odigeo Apps developer tool
 */

(function () {
    'use strict';

    /************* Settings and util functions *************/

    var URLPatterns = ["*://*/travel*"],
        qaModeURLPatterns = ["*://*.services.odigeo.com/*QaMode*"];

    function getBaseURL(url) {
        var parser = document.createElement('a');
        parser.href = url;
        return "http://" + parser.hostname +
        //      http://    www.example.com";
            ((parser.port !== "") ?  ":" + parser.port : "") + "/travel/";
        //                                 [:8080]              <ctx>/
    }

    function getProtocol(url) {
        var parser = document.createElement('a');
        parser.href = url;
        return parser.protocol.slice(0, -1);
    }

    /************* Handlers *************/

    function sessionOpenHandler(info, tab) {
        chrome.tabs.update(tab.id, {url: getBaseURL(tab.url) + "engineering/session/debug"});
    }

    function sessionExpireHandler(info, tab) {
        chrome.tabs.update(tab.id, {url: getBaseURL(tab.url) + "engineering/session/debug?op=expireSession"});
        setTimeout(function () {
            chrome.tabs.update(tab.id, {url: getBaseURL(tab.url)});
        }, 100);
    }

    function changeFlowHandler(info, tab) {
        var flows = {};
        flows[changeFlowDesktopItem] = "desktop";
        flows[changeFlowMobiletItem] = "mobile";
        flows[changeFlowGenerictItem] = "generic";
        chrome.tabs.update(tab.id, {url: getBaseURL(tab.url) + "?flow=" + flows[info.menuItemId]});
        setTimeout(function () {
            chrome.tabs.update(tab.id, {url: getBaseURL(tab.url)});
        }, 100);
    }

    function changeWebsiteHandler(info, tab) {
        var websites = {};

        websites[changeWebsiteEDITItem] = "desktop.edreams.it";
        websites[changeWebsiteGOFRItem] = "desktop.govoyages.com";
        websites[changeWebsiteOPUKItem] = "desktop.opodo.co.uk";
        websites[changeWebsiteOPGBItem] = "desktop.opodo.com";
        websites[changeWebsiteOPDEItem] = "desktop.opodo.de";
        websites[changeWebsiteOPFRItem] = "desktop.opodo.fr";
        websites[changeWebsiteOPITItem] = "desktop.opodo.it";

        chrome.tabs.update(tab.id, {url: getBaseURL(tab.url) + "?toUrl=" + websites[info.menuItemId]});
        setTimeout(function () {
            chrome.tabs.update(tab.id, {url: getBaseURL(tab.url)});
        }, 100);
    }

    function changeConnectionHandler(info, tab) {
        var connection = {};
        connection[changeConnectionDefaultItem] = "";
        connection[changeConnectionProductionItem] = "ProductionConnection";
        connection[changeConnectionMockItem] = "MockConnection";
        connection[changeConnectionTestItem] = "TestConnection";

        chrome.tabs.update(tab.id, {url: getBaseURL(tab.url) + "engineering/session/debug?op=expireSession"});
        setTimeout(function () {
            chrome.tabs.update(tab.id, {url: getBaseURL(tab.url) + "engineering/session/debug?op=changeConnection&connection=" + connection[info.menuItemId]});
            setTimeout(function () {
                chrome.tabs.update(tab.id, {url: getBaseURL(tab.url)});
            }, 100);
        }, 100);
    }

    function populateLocalStorageHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: getProtocol(tab.url) === 'http' ? "storage.js" : "secure_storage.js"
        });
    }

    function widgetInspectionHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: info.menuItemId === widgetInspectionOnItem ? "debug_widgets_on.js" : "debug_widgets_off.js"
        });
    }

    function qaModeSetCookie(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: 'qamode_cookie.js'
        });
    }

    function qaModeConfirmedHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: 'qamode_set_confirmed.js'
        });
    }

    function qaModeRetryHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: 'qamode_set_retry.js'
        });
    }

    function qaModeOFConfirmedHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: 'qamodeof_confirmed.js'
        });
    }

    function qaModeOFRetryHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: 'qamodeof_retry.js'
        });
    }

    function qaModeOFRepricingHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: 'qamodeof_repricing.js'
        });
    }

    function paxFillHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: 'pax_fill.js'
        });
    }

    function paxFillAndContinueHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: 'pax_fill.js'
        });
        chrome.tabs.executeScript(tab.id, {
            file: 'page_continue.js'
        });
    }

    function payFillHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: 'pay_fill.js'
        });
    }

    function payFillAndContinueHandler(info, tab) {
        chrome.tabs.executeScript(tab.id, {
            file: 'pay_fill.js'
        });
        chrome.tabs.executeScript(tab.id, {
            file: 'page_continue.js'
        });
    }

    /************* Menu items *************/

    var sessionOpenItem = chrome.contextMenus.create({
        "title": "Open session page",
        "contexts": ['page'],
        "onclick": sessionOpenHandler,
        "documentUrlPatterns": URLPatterns
    });
    var exprireSessionItem = chrome.contextMenus.create({
        "title": "Expire session",
        "contexts": ['page'],
        "onclick": sessionExpireHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeFlowItem = chrome.contextMenus.create({
        "title": "Change flow",
        "documentUrlPatterns": URLPatterns
    });
    var changeFlowDesktopItem = chrome.contextMenus.create({
        "title": "desktop",
        "type": "radio",
        "parentId": changeFlowItem,
        "onclick": changeFlowHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeFlowMobiletItem = chrome.contextMenus.create({
        "title": "mobile",
        "type": "radio",
        "parentId": changeFlowItem,
        "onclick": changeFlowHandler,
        "documentUrlPatterns": URLPatterns
    });
    var changeFlowGenerictItem = chrome.contextMenus.create({
        "title": "generic",
        "type": "radio",
        "parentId": changeFlowItem,
        "onclick": changeFlowHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeWebsiteItem = chrome.contextMenus.create({
        "title": "Change website",
        "documentUrlPatterns": URLPatterns
    });

    var changeWebsiteEDITItem = chrome.contextMenus.create({
        "title": "EDIT",
        "type": "radio",
        "parentId": changeWebsiteItem,
        "onclick": changeWebsiteHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeWebsiteGOFRItem = chrome.contextMenus.create({
        "title": "GOFR",
        "type": "radio",
        "parentId": changeWebsiteItem,
        "onclick": changeWebsiteHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeWebsiteOPUKItem = chrome.contextMenus.create({
        "title": "OPUK",
        "type": "radio",
        "parentId": changeWebsiteItem,
        "onclick": changeWebsiteHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeWebsiteOPGBItem = chrome.contextMenus.create({
        "title": "OPGB",
        "type": "radio",
        "parentId": changeWebsiteItem,
        "onclick": changeWebsiteHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeWebsiteOPDEItem = chrome.contextMenus.create({
        "title": "OPDE",
        "type": "radio",
        "parentId": changeWebsiteItem,
        "onclick": changeWebsiteHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeWebsiteOPFRItem = chrome.contextMenus.create({
        "title": "OPFR",
        "type": "radio",
        "parentId": changeWebsiteItem,
        "onclick": changeWebsiteHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeWebsiteOPITItem = chrome.contextMenus.create({
        "title": "OPIT",
        "type": "radio",
        "parentId": changeWebsiteItem,
        "onclick": changeWebsiteHandler,
        "documentUrlPatterns": URLPatterns
    });


    var changeConnectionItem = chrome.contextMenus.create({
        "title": "Change Connection",
        "documentUrlPatterns": URLPatterns
    });

    var changeConnectionDefaultItem = chrome.contextMenus.create({
        "title": "No override",
        "type": "radio",
        "parentId": changeConnectionItem,
        "onclick": changeConnectionHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeConnectionProductionItem = chrome.contextMenus.create({
        "title": "Production",
        "type": "radio",
        "parentId": changeConnectionItem,
        "onclick": changeConnectionHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeConnectionMockItem = chrome.contextMenus.create({
        "title": "Mock",
        "type": "radio",
        "parentId": changeConnectionItem,
        "onclick": changeConnectionHandler,
        "documentUrlPatterns": URLPatterns
    });

    var changeConnectionTestItem = chrome.contextMenus.create({
        "title": "Test",
        "type": "radio",
        "parentId": changeConnectionItem,
        "onclick": changeConnectionHandler,
        "documentUrlPatterns": URLPatterns
    });

    var populateLocalStorageItem = chrome.contextMenus.create({
        "title": "Populate local Storage",
        "contexts": ['page'],
        "onclick": populateLocalStorageHandler,
        "documentUrlPatterns": URLPatterns
    });

    var widgetInspectionItem = chrome.contextMenus.create({
        "title": "Inspect page widgets",
        "contexts": ['page'],
        "documentUrlPatterns": URLPatterns
    });


    var widgetInspectionOnItem = chrome.contextMenus.create({
        "title": "Enable/refresh",
        "parentId": widgetInspectionItem,
        "onclick": widgetInspectionHandler,
        "documentUrlPatterns": URLPatterns
    });

    var widgetInspectionOffItem = chrome.contextMenus.create({
        "title": "Disable",
        "parentId": widgetInspectionItem,
        "onclick": widgetInspectionHandler,
        "documentUrlPatterns": URLPatterns
    });

    var qaModeOFItem = chrome.contextMenus.create({
        "title": "QaMode",
        "documentUrlPatterns": URLPatterns
    });

    var qaModeOFConfirmedItem = chrome.contextMenus.create({
        "title": "Set Confirmed",
        "parentId": qaModeOFItem,
        "onclick": qaModeOFConfirmedHandler,
        "documentUrlPatterns": URLPatterns
    });

    var qaModeOFRetryItem = chrome.contextMenus.create({
        "title": "Set Payment Retry",
        "parentId": qaModeOFItem,
        "onclick": qaModeOFRetryHandler,
        "documentUrlPatterns": URLPatterns
    });

    var qaModeOFRepricingItem = chrome.contextMenus.create({
        "title": "Set Repricing at confirmBooking",
        "parentId": qaModeOFItem,
        "onclick": qaModeOFRepricingHandler,
        "documentUrlPatterns": URLPatterns
    });


    var qaModeItem = chrome.contextMenus.create({
        "title": "QaMode",
        "documentUrlPatterns": qaModeURLPatterns
    });

    var qaModeSetCookieItem = chrome.contextMenus.create({
        "title": "Set JSESSIONID in cookie",
        "parentId": qaModeItem,
        "onclick": qaModeSetCookie,
        "documentUrlPatterns": qaModeURLPatterns
    });

    var qaModeConfirmedItem = chrome.contextMenus.create({
        "title": "Set Confirmed",
        "parentId": qaModeItem,
        "onclick": qaModeConfirmedHandler,
        "documentUrlPatterns": qaModeURLPatterns
    });

    var qaModeRetryItem = chrome.contextMenus.create({
        "title": "Set Payment Retry",
        "parentId": qaModeItem,
        "onclick": qaModeRetryHandler,
        "documentUrlPatterns": qaModeURLPatterns
    });

    var paxItem = chrome.contextMenus.create({
        "title": "Passengers",
        "documentUrlPatterns": URLPatterns
    });

    var paxFillItem = chrome.contextMenus.create({
        "title": "Fill passengers data",
        "parentId": paxItem,
        "onclick": paxFillHandler,
        "documentUrlPatterns": URLPatterns
    });

    var paxFillAndContinueItem = chrome.contextMenus.create({
        "title": "Fill passengers data and continue",
        "parentId": paxItem,
        "onclick": paxFillAndContinueHandler,
        "documentUrlPatterns": URLPatterns
    });

    var payItem = chrome.contextMenus.create({
        "title": "Payment",
        "documentUrlPatterns": URLPatterns
    });

    var payFillItem = chrome.contextMenus.create({
        "title": "Fill Credit Card data",
        "onclick": payFillHandler,
        "parentId": payItem,
        "documentUrlPatterns": URLPatterns
    });
    var payFillAndContinueItem = chrome.contextMenus.create({
        "title": "Fill Credit Card data and Continue",
        "parentId": payItem,
        "onclick": payFillAndContinueHandler,
        "documentUrlPatterns": URLPatterns
    });
}());

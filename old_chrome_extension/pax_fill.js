var passengersWidgets = document.getElementsByClassName('form_content'),
    numWidgets = passengersWidgets.length, usedFakePaxs = [],
    paxNames = {
        0: {'name': 'Valentino', 'lastName': 'Rossi'},
        1: {'name': 'Casey', 'lastName': 'Stoner'},
        2: {'name': 'Jorge', 'lastName': 'Lorenzo'},
        3: {'name': 'Daniel', 'lastName': 'Pedrosa'},
        4: {'name': 'Giacomo', 'lastName': 'Agostini'},
        5: {'name': 'Angel', 'lastName': 'Nieto'},
        6: {'name': 'Maverick', 'lastName': 'Viñales'},
        7: {'name': 'Marc', 'lastName': 'Marquez'},
        8: {'name': 'Mick', 'lastName': 'Doohan'},
        9: {'name': 'Alex', 'lastName': 'Criville'},
        10: {'name': 'Wayne', 'lastName': 'Rainey'},
        11: {'name': 'Kevin', 'lastName': 'Schwantz'},
        12: {'name': 'Kenny', 'lastName': 'Roberts'},
        13: {'name': 'Randy', 'lastName': 'Mamola'},
        14: {'name': 'John', 'lastName': 'Kocinsky'},
        15: {'name': 'Fausto', 'lastName': 'Gressini'},
        16: {'name': 'Loris', 'lastName': 'Capirossi'},
        17: {'name': 'Sito', 'lastName': 'Pons'},
        18: {'name': 'Sete', 'lastName': 'Gibernau'},
        19: {'name': 'Andrea', 'lastName': 'Iannone'}
    },
    getRandomPaxNumber = function () {
        var paxId = parseInt(Math.random() * 20);
        if (usedFakePaxs.indexOf(paxId) < 0) {
            usedFakePaxs.push(paxId);
            return paxId;
        } else {
            return getRandomPaxNumber();
        }
    },
    getRandomPassenger = function () {
        return paxNames[getRandomPaxNumber()];
    };

for (var w=0; w<numWidgets; w++) {

    var inputs = passengersWidgets[w].querySelectorAll('input[data-name]'),
        selects = passengersWidgets[w].querySelectorAll('select[data-name]'),
        paxId = 0, paxType, fakePax = getRandomPassenger();

    for (var i=0; i<inputs.length; i++) {
        var input = inputs[i],
            fieldType = input.dataset.name;

        switch(fieldType) {
            case 'travellerType':
                paxType = input.value;
                break;
            case 'travellerId':
                paxId = Number.parseInt(input.value);
                break;
            case 'name':
                input.value = fakePax.name;
                break;
            case 'firstLastName':
                input.value = fakePax.lastName;
                break;
            case 'secondLastName':
                input.value = 'Segundo';
                break;
            case 'travellerGender':
                input.value = 'MALE';
                break;
            case 'dateOfBirth':
                switch (paxType) {
                    case 'INFANT':
                        input.value = '2016-01-01';
                        break;
                    case 'CHILD':
                        input.value = '2014-01-01';
                        break;
                    default:
                        input.value = '1986-01-01';
                }
                break;
            case 'mail':
                input.value = 'test@mail.com';
                break;
            case 'validationMail':
                input.value = 'test@mail.com';
                break;
            case 'address':
                input.value = 'addresssssssssss';
                break;
            case 'cityName':
                input.value = 'cityyyyyyyyyy';
                break;
            case 'zipCode':
                input.value = '18000';
                break;
            case 'country':
                input.value = 'ES';
                break;
            case 'phoneCode1':
                input.value = 'ES';
                break;
            case 'phoneNumber1':
                input.value = '600123456';
                break;
            case 'stateName':
                input.value = 'CA';
                break;
            case 'identification':
                input.value = '47349582B';
                break;
        }
    }

    for (var j=0; j<selects.length; j++) {
        var select = selects[j],
            fieldType = select.dataset.name;

        switch(fieldType) {
            case 'travellerGender':
                select.value = 'MALE';
                break;
        }
    }
}

//Email on top
var setMail = function (selector) {
    selector.focus();

    selector.value = 'test@mail.com';

    var changeEvent = document.createEvent("HTMLEvents");
    changeEvent.initEvent("change", true, true);
    selector.dispatchEvent(changeEvent);

    selector.blur();
};

var emailOnTop = document.getElementById('user-identification');
if(emailOnTop !== null) {
    var mailInput = emailOnTop.querySelector("input[data-name='mail']");
    setMail(mailInput);

    mailInput = emailOnTop.querySelector("input[data-name='validationMail']");
    setMail(mailInput);
}

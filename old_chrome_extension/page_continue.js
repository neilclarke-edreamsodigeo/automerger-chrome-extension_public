var skipNags = function () {
    // Skip nags
    var nagSkipButton = document.getElementsByClassName('od-nagbutton-secondary-test');
    if (nagSkipButton.length > 0) {
        nagSkipButton[0].click();
    }
};


// Passengers desktop
var legalCheck = document.getElementById('details-pax-legalcheck');
if (!!legalCheck) {
    legalCheck.checked = true;
}

var detailsPageButton = document.getElementsByClassName('details_continue_button');
if (detailsPageButton.length > 0) {
    detailsPageButton[0].click();

    window.setTimeout(skipNags, 2000);
}

// Passengers mobile
var detailsPageButton = document.getElementsByClassName('passengers_continue_btn');
if (detailsPageButton.length > 0) {
    detailsPageButton[0].click();
}

// Payment desktop
var paymentPageButton = document.getElementsByClassName('payment_continue_button');
if (paymentPageButton.length > 0) {
    paymentPageButton[0].click();
}



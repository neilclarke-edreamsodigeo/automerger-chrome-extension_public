#!/usr/bin/env node

'use strict';

const svgexport = require('svgexport');
const datafile = [16, 48, 128].map((px) => ({
    'input': ['script/onefront-chrome-extension.svg'],
    'output': [[`script/icon${px}.png`, `${px}:${px}`]],
}));

svgexport.render(datafile);

(function() {
    'use strict';
    module.exports = function(grunt) {
        const versionSuffix = grunt.option('versionSuffix') || 0;
        let odiMergerJsFiles = ['*.js', 'data/*.js', 'page/*.js', 'util/*.js', 'bot/*.js'];
        const pkg = require('./package.json');
        const templateSettings = {
            match: pkg.match,
            icon: pkg.icon,
            name: pkg.name,
            namespace: pkg.namespace,
            version: pkg.version,
            grant: pkg.grant,
            description: pkg.description,
            author: pkg.author,
            updateURL: pkg.update,
        };
        const getBanner = function() {
            const linebreak = '\n';
            let header = '// ==UserScript==' + linebreak; let settings;
            for (settings in templateSettings) {
                if (templateSettings.hasOwnProperty(settings)) {
                    header += '// @' + settings + ' ' + templateSettings[settings] + linebreak;
                }
            }
            return header + '// ==/UserScript==';
        };

        grunt.initConfig({
            pkg: require('./package.json'),
            bump: {
                options: {
                    files: ['package.json'],
                    updateConfigs: [],
                    commit: false,
                    createTag: false,
                    push: false,
                    globalReplace: false,
                    prereleaseName: false,
                    metadata: '',
                    regExp: false,
                },
            },
            header: '',
            usebanner: {
                taskName: {
                    options: {
                        position: 'top',

                        banner: getBanner(),
                        linebreak: true,
                    },
                    files: {
                        src: ['script/release.js'],
                    },
                },
            },
            browserify: {
                vendor: {
                    src: 'index.js',
                    dest: 'build/release.js',
                    options: {
                        // require: ['jquery', 'underscore'],
                        transform: ['imgurify'],
                    },
                },
            },
            eslint: {
                target: odiMergerJsFiles,
                options: {

                },
            },
            uglify: {
                odimerger: {
                    files: {
                        'script/release.js': ['build/release.js'],
                    },
                },
            },
            replace_json: {
                manifest: {
                    src: './script/manifest.json',
                    changes: {
                        'version': `${pkg.version}.${versionSuffix}`,
                    },
                },
            },
            zip: {
                'extension': {
                    cwd: './script/',
                    src: ['./script/*'],
                    dest: './dist/extension.zip',
                },
            },
        });

        grunt.loadNpmTasks('grunt-eslint');
        grunt.loadNpmTasks('grunt-browserify');
        grunt.loadNpmTasks('grunt-contrib-uglify-es');
        grunt.loadNpmTasks('grunt-bump');
        grunt.loadNpmTasks('grunt-banner');
        grunt.loadNpmTasks('grunt-replace-json');
        grunt.loadNpmTasks('grunt-zip');

        grunt.registerTask('ver', ['bump']);
        grunt.registerTask('lint', ['eslint']);
        grunt.registerTask('default', ['eslint', 'browserify', 'uglify', 'usebanner']);
        grunt.registerTask('build', ['default', 'replace_json', 'zip']);
    };
}());

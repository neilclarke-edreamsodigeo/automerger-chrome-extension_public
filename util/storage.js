(function() {
    'use strict';
    const Odimergie = {};
    const _ = require('underscore');
    Odimergie.Settings = require('../data/settings');
    module.exports = {
        cookieName: 'odiMerge',
        load: function() {
            let storedSetting = this.getCookie(this.cookieName);
            let finalSettings = Odimergie.Settings;
            if (!_.isEmpty(storedSetting)) {
                storedSetting = JSON.parse(storedSetting);
                finalSettings = storedSetting.version < Odimergie.Settings.version ? finalSettings : storedSetting;
            }
            return finalSettings;
        },
        delete: function() {
            this.deleteCookie(this.cookieName);
        },
        save: function(settings) {
            if (settings) {
                this.deleteCookie(this.cookieName);
                this.setCookie(this.cookieName, JSON.stringify(settings), 360);
            }
        },
        // 3rd party Methods
        getCookie: function(cName) {
            if (document.cookie.length > 0) {
                let cStart = document.cookie.indexOf(cName + '=');
                if (cStart !== -1) {
                    cStart = cStart + cName.length + 1;
                    let cEnd = document.cookie.indexOf(';', cStart);
                    if (cEnd === -1) {
                        cEnd = document.cookie.length;
                    }
                    return decodeURIComponent(document.cookie.substring(cStart, cEnd));
                }
            }
            return '';
        },
        setCookie: function(name, value, days) {
            let expires;
            if (days) {
                let date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = '; expires=' + date.toGMTString();
            } else {
                expires = '';
            }
            document.cookie = name + '=' + value + expires + '; path=/';
        },
        deleteCookie: function(name) {
            document.cookie = name + '=' + '; expires=Thu, 01 Jan 1970 00:00:01 GMT';
        },
    };
})();

module.exports = {
    HUB: require('../page/hub'),
    pullRequestList: require('../page/pullRequestList'),
    pullRequest: require('../page/pullRequest'),
    CI: require('../page/ci'),
    createPullRequest: require('../page/createPullRequest'),
    Autos: require('../page/autos'),
};

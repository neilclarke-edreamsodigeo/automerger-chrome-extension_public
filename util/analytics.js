(function() {
    'use strict';
    let _ = require('underscore');
    const analytics = {
        /*
         * Rules Analytics:
         * eventCategory: Page (Bitbucket, Jira, Jenkins CI, Jenkins Autos)
         * eventAction: Action (Merge, approve, decline, ...)
         * eventLabel: Project (OneFront, Automerger, eDreamsSite,...)
         */

        /*
         * Dimensions Map
         * 1 - pod
         * 2 - ticket
         * 3 - user
         * 4 - forkName
         * 5 - rulesPassed
         * 6 - rulesFailed
         * 7 - rulesWarning
         * 8 - prod
         */
        addTracker: function(script) {
            let scr = document.createElement('script');
            scr.type = 'text/javascript';
            scr.textContent = script;
            (document.head || document.body || document.documentElement).appendChild(scr);
        },

        tracking: function(settings) {
            let trackData = JSON.stringify(settings);
            let name = JSON.stringify('odigea' + new Date().getTime());
            let analyticsInitialization = '(' + function(trackData, name) {
                window.ga('create', 'UA-99140371-1', 'auto', name);
                window.ga(name + '.send', trackData);
            } + ')(' + trackData + ', ' + name + ');';
            this.addTracker(analyticsInitialization);
        },
        trackingEvent: function(settings, dimensions) {
            let trackData = _.extend(settings, dimensions);
            this.tracking(_.extend(trackData, {
                'hitType': 'event',
            }));
        },
        trackingMerge: function(pod, tickets, user, forkName, rulesPassed, rulesFailed, rulesWarning, prod) {
            let dimensions = {
                'dimension1': pod,
                'dimension2': '',
                'dimension3': user,
                'dimension4': forkName,
                'dimension5': rulesPassed,
                'dimension6': rulesFailed,
                'dimension7': rulesWarning,
                'dimension8': prod,
            };
            _.forEach(tickets, function(ticket) {
                dimensions.dimension2 = ticket;
                analytics.trackingEvent({
                    'eventCategory': 'Bitbucket',
                    'eventAction': 'Merge',
                    'eventLabel': 'OneFront',
                }, dimensions);
            }, this);
        },
    };
    module.exports = analytics;
})();

(function() {
    'use strict';
    const _ = require('underscore');
    const regex = require('../data/regExp');
    const constants = require('../data/constants');

    module.exports = {
        colorMapStatus: {
            'SUCCESS': 'odm-rule-passed',
            'FAIL': 'odm-rule-failed',
            'true': 'odm-rule-passed',
            'TRUE': 'odm-rule-passed',
            'false': 'odm-rule-failed',
            'FALSE': 'odm-rule-failed',
            'BUILDING': 'odm-rule-warning',
            'OUTDATED': 'odm-rule-failed',
            'UPDATED': 'odm-rule-passed',
            'MINOR': 'odm-rule-warning',
        },
        textMapStatus: {
            'false': 'FAIL',
            'true': 'SUCCESS',
            'SUCCESS': 'SUCCESS',
            'TRUE': 'SUCCESS',
            'FAIL': 'FAIL',
            'BUILDING': 'BUILDING',
            'OUTDATED': 'FAIL',
            'UPDATED': 'SUCCESS',
            'MINOR': 'WARNING',
        },
        getToday: function() {
            return new Date();
        },
        getSeconds: function(seconds) {
            return seconds * 1000;
        },
        getMinutes: function(minutes) {
            return this.getSeconds(60) * minutes;
        },
        getHours: function(hours) {
            return this.getMinutes(60) * hours;
        },
        getDays: function(days) {
            return this.getHours(24) * days;
        },
        getDaysBetweenDates: function(date1, date2) {
            let timeDiff = Math.abs(date2.getTime() - date1.getTime());
            return Math.ceil(timeDiff / this.getDays(1));
        },

        getApiDataUrl: function(url) {
            return url.replace(/\/$/, '').concat('/api/json');
        },
        compareCommits: function(commit, ciInfo) {
            return commit && commit[0] && commit[0].href && commit[0].href.split('/').length > 6 &&
                    commit[0].href.split('/')[6] === ciInfo.commitId;
        },
        parseVersionToInt: function(release, minor) {
            return parseInt((release + '' + minor), 10);
        },
        getVersionStatus: function(local, last) {
            let tempSplitLocal = local.split('.');
            let tempSplitLast = last.split('.');
            let localVersion = this.parseVersionToInt(tempSplitLocal[0], tempSplitLocal[1]);
            let lastVersion = this.parseVersionToInt(tempSplitLast[0], tempSplitLast[1]);
            let status = 'UPDATED';
            if (localVersion < lastVersion) {
                status = 'OUTDATED';
            } else if (localVersion === lastVersion && tempSplitLocal[2] < tempSplitLast[2]) {
                status = 'MINOR';
            }
            return status;
        },
        extractAutosLink(autosJnk) {
            if (autosJnk && autosJnk.children() && autosJnk.children()[0] && autosJnk.children()[0].href) {
                return autosJnk.children()[0].href;
            } else if (autosJnk && autosJnk.children() && autosJnk.children().length > 1) {
                return autosJnk.children()[1].href;
            }
        },
        extractAutosData: function(data) {
            let newData;
            let getParametersFromActions = function(actions) {
                let action = _.find(actions, function(action) {
                    if (action._class === 'hudson.model.ParametersAction') {
                        return true;
                    }
                });
                return action ? action.parameters : {};
            };
            let extractParameters = function(data) {
                let parameters = getParametersFromActions(data.actions);
                let formattedData = {};
                _.forEach(parameters, function(parameter) {
                    if (parameter.name && parameter.value) {
                        formattedData[parameter.name] = parameter.value;
                    }
                });
                return formattedData;
            };
            newData = extractParameters(data);
            newData.status = data.result === 'SUCCESS' && !data.building;
            newData.lastUpdate = this.getDaysBetweenDates(new Date(data.timestamp), this.getToday());
            return newData;
        },
        getCssColorStatus: function(status) {
            return this.colorMapStatus[status.toString()];
        },
        getTextStatus: function(status) {
            return this.textMapStatus[status.toString()];
        },
        copyToClipboard: function(textoToCopy) {
            window.prompt('The current fixVersion is: ', textoToCopy);
        },
        getJenkinsVersion: () => window.localStorage.getItem('PRData-JenkinsType') || 'AREA',

        isJenkinsType: (type) => window.localStorage.getItem('PRData-JenkinsType') === type,

        setJenkinsType: (type) => window.localStorage.setItem('PRData-JenkinsType', type),

        getForkName: () => {
            const url = window.location.pathname;
            const [, project] = regex.forkRegex.exec(url);
            return project;
        },

        getUpstreamName: () => {
            const fork = module.exports.getForkName();
            const [project] = fork.split('_');
            return project;
        },

        isOneFront: () => module.exports.getForkName().indexOf(constants.UPSTREAM) !== -1,

    };
})();

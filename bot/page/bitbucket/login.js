let credentials = require('../../credentials.js');
module.exports = {
    url: 'https://bitbucket.org/account/signin/',
    locators: {
        username: '#js-email-field',
        password: '#js-password-field',
        submit: '.selenium-login-submit',
    },
    doLogin: function(client) {
        'use strict';
        client
            .url(this.url)
            .setValue(this.locators.username, credentials.bitbucket.user)
            .setValue(this.locators.password, credentials.bitbucket.password)
            .click(this.locators.submit);
    },
};

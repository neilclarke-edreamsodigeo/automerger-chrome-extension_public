const Constants = require('../data/constants');

function getJiraLinkField() {
    return `<div class="od-ticket-info">
                <h2 class="odm-h2"> Ticket info </h2>
                <div class="field-group">
                    <label> Jira ticket wasn't found. Please, insert it! </label>
                    <input type="text" class="jira_ticket_input" placeholder="https://jira.odigeo.com/browse/TV-245">
                </div>
            </div>`;
}

module.exports = function(jiraLink) {
    let jiraLinkField = !jiraLink ? getJiraLinkField() : '';

    return `<div class="od-container">
    <h1 class="odm-h1"> PR AUTOS TEMPLATE GENERATOR
     <span class="odm-toggle-tool-btn toggle_tool" data-state="false"> 
        <span class="odm-toggle-tool-open-icon toggle_open_icon"> 🔽 </span>
         <span class="odm-toggle-tool-close-icon odm-hidden toggle_close_icon"> 🔼 </span> 
     </span> 
    </h1>
    <div class="odm-pr-generator-tool-container tool_main_container">
        <div class="aui-button aui-button-primary odm-button-warning dismiss_pr_tool" data-show-tool="true" title="DISMISS PR TOOL">
          ${Constants.CMS.DISMISS_TOOL_DISCLAIMER}
        </div>
        ${jiraLinkField}        
        <div class="area_jenkins_selector_wrapper">
            Select your area
            <select class="area_jenkins_selector">
                <option value="">Please select your area</option>
            </select>
        </div>
        <div class="od-autos-ids">
            <div class="autos_container" data-device="main">
                <h2 class="odm-h2"> Autos </h2>
                <div class="field-group">
                    <label> Main ID </label>
                    <input type="text" class="main_id" placeholder="13780"> </div>
                <div class="buttons-container group">
                    <div class="odm-buttons-group">
                        <div class="aui-button aui-button-primary add_failed_auto_jenkins" data-fail-type-button="0" title="Add Jenkins Fail">
                            ${Constants.CMS.AUTOS_ICONS[0]}<span class="odm-btn-tooltip"> Add Jenkins Fail </span>
                        </div>
                        <div class="aui-button aui-button-primary add_failed_auto_known" data-fail-type-button="1" title="Add known issue">
                            ${Constants.CMS.AUTOS_ICONS[1]}<span class="odm-btn-tooltip"> Add known issue </span>
                        </div>
                        <div class="aui-button aui-button-primary add_failed_auto_unknown" data-fail-type-button="2" title="Add Unknown issue">
                            ${Constants.CMS.AUTOS_ICONS[2]}<span class="odm-btn-tooltip"> Add Unknown issue </span>
                        </div>
                    </div>
                </div>
                <div class="failed_autos">
                    <div class="failed_autos_tabs_nav"> </div>
                    <div class="failed_autos_tabs_content_container"></div>
                </div>
            </div>
        </div>
        <div class="buttons-container group odm-manage-template-buttons-container">
            <div class="odm-buttons-group">
                <p class="odm-buttons-group-title"> Manage template data</p>
                <div class="aui-button aui-button-primary save_pr_data" title="Save PR data">
                    ${Constants.CMS.GENERIC_ICONS.SAVE_PR_DATA}<span class="odm-btn-tooltip"> ${Constants.CMS.SAVE_PR_DATA}</span>
                </div>
                <div class="aui-button aui-button-primary generate_template" title="Generate template">
                    ${Constants.CMS.GENERIC_ICONS.GENERATE_TEMPLATE}<span class="odm-btn-tooltip">${Constants.CMS.GENERATE_TEMPLATE}</span>
                </div>
                <a class="aui-button aui-button-primary export_txt" title="Export Template in txt">
                    ${Constants.CMS.GENERIC_ICONS.EXPORT}<span class="odm-btn-tooltip">${Constants.CMS.EXPORT_TXT}</span>
                </a>
                <a class="aui-button aui-button-primary export_txt_jira" title="Export Template for Jira">
                    ${Constants.CMS.GENERIC_ICONS.EXPORT}Jira <span class="odm-btn-tooltip"> ${Constants.CMS.EXPORT_TXT_JIRA}</span>
                </a>
            </div>
            <div class="odm-buttons-group">
                <p class="odm-buttons-group-title"> Delete template data</p>
                <div class="aui-button aui-button-primary odm-button-danger remove_this_fork_data" title="DELETE THIS FORK DATA">
                    ${Constants.CMS.GENERIC_ICONS.REMOVE_PR_DATA}THIS FORK<span class="odm-btn-tooltip"> ${Constants.CMS.DELETE_DATA_FORK}</span>
                </div>
                <div class="aui-button aui-button-primary odm-button-danger remove_all_forks_data" title="DELETE ALL FORKS DATA">
                    ${Constants.CMS.GENERIC_ICONS.REMOVE_PR_DATA}ANY FORK<span class="odm-btn-tooltip"> ${Constants.CMS.DELETE_DATA_ALL_FORKS}</span>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="failed_auto_jenkins_tmpl">
    <div class="failed_auto" data-fail-type="0">
        <div class="odm-tab-content failed_autos_tab_content" id="<%=tabId %>">
            <h3 class="odm-h3"> Jenkins fail </h3>
            <div class="buttons-container group">
                <div class="aui-button aui-button-primary odm-button-danger remove_fail"> Remove fail </div>
            </div>
            <div class="field-group">
                <label> Browser </label>
                <input list="jenkins_failed_browser" class="auto_browser_input">
                <datalist class="odm-select-input auto_browser" id="jenkins_failed_browser">
                    <option value='chrome'>chrome</option>
                    <option value='chromeMobile'>chromeMobile</option>
                </datalist>
            </div>
            <div class="field-group">
                <label> Feature </label>
                <input type="text" class="auto_feature" placeholder="activation_account.feature" value="<%=feature %>"> </div>
            <div class="field-group">
                <label> Seed </label>
                <input type="text" class="auto_seed" placeholder="12461314" value="<%=seed %>"> </div>
            <div class="field-group">
                <label> Single </label>
                <input type="text" class="auto_single" placeholder="779" value="<%=single %>"> </div>
            <div class="field-group">
                <label> Comments </label>
                <textarea class="auto_desc_field">
                    <%=comments %>
                </textarea>
            </div>
        </div>
    </div>
</script>
<script type="text/template" id="failed_auto_tab_link_tmpl">
    <div class="odm-tab-link failed_autos_tab_link" data-tab-content-id="<%=tabId %>">
        <%=tabText %>
    </div>
</script>
<script type="text/template" id="failed_auto_known_issue_tmpl">
    <div class="failed_auto" data-fail-type="1">
        <div class="odm-tab-content failed_autos_tab_content" id="<%=tabId %>">
            <h3 class="odm-h3"> Known issue fail </h3>
            <div class="buttons-container group">
                <div class="aui-button aui-button-primary odm-button-danger remove_fail"> Remove fail </div>
            </div>
            <div class="field-group">
                <label> Browser </label>
                <input list="jenkins_failed_browser" class="auto_browser_input">
                <datalist class="odm-select-input auto_browser" id="jenkins_failed_browser">
                    <option value='chrome'>chrome</option>
                    <option value='chromeMobile'>chromeMobile</option>
                </datalist>
            </div>
            <div class="field-group">
                <label> Feature </label>
                <input type="text" class="auto_feature" placeholder="activation_account.feature" value="<%=feature %>"> </div>
            <div class="field-group">
                <label> Ticket </label>
                <input type="text" class="auto_ticket" placeholder="OUA-XYZ" value="<%=ticket %>"> </div>
            <div class="field-group">
                <label> Comments </label>
                <textarea class="auto_desc_field">
                    <%=comments %>
                </textarea>
            </div>
        </div>
    </div>
</script>
<script type="text/template" id="failed_auto_unknown_issue_tmpl">
    <div class="failed_auto" data-fail-type="2">
        <div class="odm-tab-content failed_autos_tab_content" id="<%=tabId %>">
            <h3 class="odm-h3"> Unknown issue </h3>
            <div class="buttons-container group">
                <div class="aui-button aui-button-primary odm-button-danger remove_fail"> Remove fail </div>
            </div>
            <div class="field-group">
                <label> Browser </label>
                <input list="jenkins_failed_browser" class="auto_browser_input">
                <datalist class="odm-select-input auto_browser" id="jenkins_failed_browser">
                    <option value='chrome'>chrome</option>
                    <option value='chromeMobile'>chromeMobile</option>
                </datalist>
            </div>
            <div class="field-group">
                <label> Feature </label>
                <input type="text" class="auto_feature" placeholder="activation_account.feature" value="<%=feature %>"> </div>
            <div class="field-group">
                <label> Seed </label>
                <input type="text" class="auto_seed" placeholder="12461314" value="<%=seed %>"> </div>
            <div class="field-group">
                <label> Compare build with Seed </label>
                <input type="text" class="compare_with_seed" placeholder="779" value="<%=compareWithSeed %>"> </div>
            <div class="field-group">
                <label> Compare build without Seed (Optional) </label>
                <input type="text" class="compare_without_seed" placeholder="780" value="<%=compareWithoutSeed %>"> </div>
            <div class="field-group">
                <label> Comments </label>
                <textarea class="auto_desc_field">
                    <%=comments %>
                </textarea>
            </div>
        </div>
    </div>
</script>`;
};

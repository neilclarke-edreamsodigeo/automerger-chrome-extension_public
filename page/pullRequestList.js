(function() {
    'use strict';
    const Odimergie = {};
    const $ = require('jquery');
    const _ = require('underscore');

    Odimergie.constants = require('../data/constants');
    Odimergie.mentors = require('../data/mentors');
    Odimergie.regExp = require('../data/regExp');
    Odimergie.services = require('../data/services');
    Odimergie.validations = require('../data/validations');
    Odimergie.HUB = require('../page/hub');
    Odimergie.Collected = require('../data/collected');
    const pullRequestList = {
        pageName: Odimergie.constants.CMS.PAGENAME.PULLREQUESTLIST,
        locator: {
            own: {
                pullRequest: 'od_pullrequest',
            },
            row: '.pull-request-row',
            id: 'data-pull-request-id',
            title: '.pull-request-title',
            user: '.pull-request-author span',
            dataUser: 'username',
            // SEARCH INSIDE ELEMENT
            reviewers: ' .participant-item',
            extraReviewers: function(id) {
                return '#reviewers-' + id + ' .participant-item span';
            },
            pullRequestTitle: function(domId) {
                return '#' + domId + ' .odm-pullrequest-link a';
            },
            descriptionsID: '#pr-shared-component',
            descriptionsData: 'initial-prs',
        },
        upstreamStatus: false,
        collections: [],
        excludeList: Odimergie.constants.EXCLUDE_REVIEWER_LIST,
        isExcluded: function(name) {
            return _.some(this.excludeList, function(exclude) {
                return name === exclude;
            });
        },
        createNewCollection: function() {
            return _.extend({}, Odimergie.Collected);
        },
        setOwnerData: function(element, project) {
            let name = $(element).find(this.locator.user).data(this.locator.dataUser);
            return {
                name: name,
                isMentor: _.contains(_.pluck(Odimergie.mentors.getMentors(), 'userName'), name),
                hasMentor: _.contains(_.pluck(Odimergie.mentors.getMentors(), 'project'), project),
            };
        },
        setProjectFromTitle: function(title) {
            let tempProject = Odimergie.regExp.extractTicketCodeList(title)[0];
            return tempProject ? tempProject.split('-')[0] : '';
        },
        getForkNameFromDom: function(description) {
            let fork;
            try {
                fork = Odimergie.regExp.forkNameDescription.exec(description)[1];
            } catch (e) {
                // console.error('error');
            }
            return fork;
        },
        setElementCollection: function(element, descriptions) {
            let id = parseInt(element.attributes[this.locator.id].value, 10);
            let title = $(element).find(this.locator.title).html();
            let project = this.setProjectFromTitle(title);
            let collection = {
                id: id,
                domId: 'odimergie-' + id,
                title: title,
                project: project,
                description: descriptions[id],
                forkName: this.getForkNameFromDom(descriptions[id]),
                Autos: {
                    parallels: Odimergie.services.getAutosExecutionIdPlainDescription(descriptions[id], Odimergie.regExp.autosId),
                    singles: Odimergie.services.getAutosExecutionIdPlainDescription(descriptions[id], Odimergie.regExp.autosSingleId),
                },
                owner: this.setOwnerData(element, project),
                isPrivate: false,
                isPublic: true,
                CI: {
                    status: false,
                },
            };
            collection = _.extend(collection, this.getReviewersDataFromDOM(element, id));
            return collection;
        },
        getDomExtraReviewers: function(id) {
            return $(this.locator.extraReviewers(id));
        },
        getDomRegularReviewers: function(element) {
            return $(element).find(this.locator.reviewers);
        },
        getExtraReviewers: function(id) {
            let reviewersData = {
                who: [],
                approves: 0,
                reviewers: [],
            };
            let $reviewers = this.getDomExtraReviewers(id);
            return this.evaluateNewReviewer($reviewers, reviewersData);
        },
        evaluateNewReviewer: function(reviewers, reviewersData) {
            _.forEach(reviewers, function(reviewer) {
                let $el = $(reviewer);
                let name = $el.data('username');
                if (!pullRequestList.isExcluded(name) && typeof name === 'string') {
                    reviewersData.reviewers.push({
                        avatar: 'https://bitbucket.org/account/' + name + '/avatar/32/',
                        name: name,
                        approve: $el.find('.approved').length > 0,
                    });
                    reviewersData.who.push(name);
                    reviewersData.approves = $el.find('.approved').length > 0 ?
                        reviewersData.approves + 1 : reviewersData.approves;
                }
            }, this);
            return reviewersData;
        },
        getReviewersDataFromDOM: function(element, id) {
            let reviewersData = this.getExtraReviewers(id);
            let reviewers = this.getDomRegularReviewers(element);
            reviewersData = this.evaluateNewReviewer(reviewers, reviewersData);
            return {
                approves: {
                    quantity: reviewersData.approves,
                    who: reviewersData.who,
                },
                reviewers: reviewersData.reviewers,
            };
        },
        getDescriptionsFromDOM: function() {
            let initialsPR = $(this.locator.descriptionsID).data(this.locator.descriptionsData);
            let compactPRInfo = {};
            _.forEach(initialsPR.values, function(pullRequest) {
                compactPRInfo[pullRequest.id] = pullRequest.description;
            });
            return compactPRInfo;
        },
        getPullRequestElements: function() {
            let descriptions = this.getDescriptionsFromDOM();
            return _.map($(this.locator.row), function(element) {
                return pullRequestList.setElementCollection(element, descriptions);
            }, this);
        },
        renderOption: function(e, image, link) {
            return $('<a></a>', {
                href: link,
            })
                .addClass('odm-primary-color')
                .append($('<span></span>')
                    .append(this.imageLogo(image)));
        },
        renderJiraOptions: function(pullRequest) {
            let $jiraOptions = $('<span></span>');
            _.forEach(Odimergie.regExp.extractTicketCodeList(pullRequest.title), function(url) {
                if (!_.isUndefined(url)) {
                    $jiraOptions
                        .append(pullRequestList.renderOption(pullRequest,
                            Odimergie.HUB.urls.LOGOS.JIRA,
                            Odimergie.HUB.urls.JIRA + url));
                }
            }, this);
            return $jiraOptions;
        },
        renderBitBucketOptions: function(pullRequest) {
            return this.renderOption(pullRequest, Odimergie.HUB.urls.LOGOS.BITBUCKET, Odimergie.HUB.urls.UPSTREAM + pullRequest.id);
        },
        renderOptions: function(pullRequest) {
            return this.renderJiraOptions(pullRequest)
                .append(this.renderBitBucketOptions(pullRequest));
        },
        renderPullRequestTitle: function(pullRequest) {
            let pullRequestName;
            _.forEach(Odimergie.regExp.extractTicketCodeList(pullRequest.title), function(code) {
                if (!_.isUndefined(code)) {
                    pullRequestName += code;
                }
            });
            let ticketList = Odimergie.regExp.extractTicketCodeList(pullRequest.title);
            pullRequestName = ticketList.shift();
            if (ticketList.length) {
                pullRequestName += ' + ' + ticketList.length + ' more';
            }
            return $('<span></span>').addClass('odm-pullrequest-link ')
                .append($('<a></a>', {
                    href: Odimergie.HUB.urls.UPSTREAM + pullRequest.id,
                })
                    .append(pullRequestName ? pullRequestName : 'PR id: ' + pullRequest.id));
        },
        separator: function() {
            return $('<hr>').css({
                margin: 0,
            });
        },
        imageLogo: function(link) {
            return $('<img/>', {
                src: link,
            }).css({
                width: '20px',
            });
        },
        pullRequestRenderElement: function(element) {
            return ($('<div></div>', {
                id: element.domId,
            }).addClass(this.locator.own.pullRequest)
                .append(this.renderPullRequestTitle(element))
                .append($('<div></div>')
                    .append(this.renderOptions(element))
                )
                .append(this.separator)
            );
        },
        renderList: function() {
            let pullRequestElements = _.sortBy(this.getPullRequestElements(), 'id');
            this.collections = pullRequestElements;
            return _.map(pullRequestElements, function(element) {
                return pullRequestList.pullRequestRenderElement(element);
            }, this);
        },
        getCIValidation: function(forkName) {
            return new Promise(function(resolve) {
                Odimergie.services.getCIstatus(forkName, false, function(status) {
                    resolve(status);
                }, pullRequestList);
            });
        },
        getAutoValidations: function(element) {
            let that = this;
            return new Promise(function(resolve) {
                let autosPromises = _.map(element.Autos.parallels, function(parallel) {
                    return that.getSingleAutoValidation(parallel);
                });
                Promise.all(autosPromises).then(function(data) {
                    resolve(data);
                });
            });
        },
        getSingleAutoValidation: function(idAuto) {
            return new Promise(function(resolve) {
                Odimergie.services.getAutosStatus(idAuto,
                    function(data) {
                        resolve(data);
                    }
                );
            });
        },
        getAsyncValidations: function() {
            let that = this;
            let AutosPromises = _.map(this.collections, function(collection) {
                return that.getAutoValidations(collection);
            });
            let CIPromises = _.map(this.collections, function(collection) {
                return that.getCIValidation(collection.forkName);
            });
            let UpstreamPromise = new Promise(function(resolve) {
                Odimergie.services.getUpstreamStatus(function(status) {
                    resolve(status);
                });
            });

            Promise.all(AutosPromises)
                .then(function(data) {
                    _.each(data, function(collection, index) {
                        that.collections[index].Autos.parallels = collection;
                    });
                })
                .then(Promise.all(CIPromises).then(function(data) {
                    _.each(data, function(collection, index) {
                        that.collections[index].CI.status = collection;
                    });
                    that.renderValidations();
                }))
                .then(UpstreamPromise.then(function(data) {
                    that.upstreamStatus = data;
                }));
        },

        getUpstreamValidations: function() {
            return _.filter(Odimergie.validations, function(validation) {
                return _.isObject(validation.enable) && validation.enable['upstream'];
            });
        },

        isCollectionReadyForMerge: function(collection) {
            return !_.some(this.getUpstreamValidations(), function(validation) {
                return !validation.execute(collection) && validation.blocker;
            });
        },
        renderValidations: function() {
            _.forEach(this.collections, function(collection) {
                collection.isReadyforMerge = pullRequestList.isCollectionReadyForMerge(collection);
                $(pullRequestList.locator.pullRequestTitle(collection.domId))
                    .addClass(collection.isReadyforMerge ? 'odm-rule-passed' : 'odm-rule-failed');
            }, this);
        },
        render: function() {
            Odimergie.HUB.render()
                .append($('<div></div>', {
                    id: 'HUB-content-' + this.pageName,
                })
                    .addClass('HUB-content')
                    .append(this.renderList())
                );
            this.getAsyncValidations();
        },
    };
    module.exports = pullRequestList;
})();

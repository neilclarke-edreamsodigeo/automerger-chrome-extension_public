(function() {
    'use strict';
    const Odimergie = {};
    const $ = require('jquery');

    Odimergie.services = require('../data/services');
    Odimergie.HUB = require('../page/hub');
    Odimergie.constants = require('../data/constants');

    const settingsMenu = {
        id: 'odm_settings_overlay',
        HUBWidth: '',
        toggleOptions: function() {
            let $element = $('.' + this.id);
            if ($element.length === 0) {
                this.renderSettings();
            } else {
                $element.toggle('odm_hidden');
            }
        },
        renderSettings: function() {
            this.renderHTML('v:' + Odimergie.constants.VERSION);
        },
        getSettingsFromServices: function() {
            return new Promise(function(resolve) {
                Odimergie.services.getSettingsAutomerger(function() {
                    resolve();
                }, settingsMenu);
            });
        },
        renderHTML: function(html) {
            const HUBWidth = $('#' + Odimergie.HUB.id).width();
            $('body').append($('<div></div>', {
                id: this.id.id,
            })
                .css({
                    left: HUBWidth + Odimergie.constants.CSS.WITHNAVBARBB,
                })
                .addClass(this.id + ' ' + Odimergie.HUB.id)
                .append(html)
            );
        },
    };
    module.exports = settingsMenu;
})();

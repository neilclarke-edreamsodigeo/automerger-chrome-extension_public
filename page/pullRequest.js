(function() {
    'use strict';
    const Odimergie = {};
    const $ = require('jquery');
    const _ = require('underscore');

    Odimergie.mentors = require('../data/mentors');
    Odimergie.constants = require('../data/constants');
    Odimergie.regExp = require('../data/regExp');
    Odimergie.debugMode = require('../data/debug');
    Odimergie.HUB = require('../page/hub');
    Odimergie.util = require('../util/util');
    Odimergie.services = require('../data/services');
    Odimergie.validations = require('../data/validations');
    Odimergie.Collected = Odimergie.Collected || require('../data/collected');
    Odimergie.analytics = require('../util/analytics');

    const pullRequest = {
        locator: {
            successMerge: '.aui-lozenge-success',
            commit: '.view-file.aui-button.aui-button-light',
            autos: '.description .wiki-content a',
            forkName: '#id_source_group .repo-name',
            reviewers: '.reviewers-group .participants li',
            title: '.pull-request-title',
            conflicts: '.diff-summary-lozenge[original-title^="Conflict"]',
            gutter: '.gutter',
            editComment: '.reply-link',
            generalComments: '#comments-list',
            comment: '.comment',
            submitButton: '[type="submit"]',
            customLocator: '.od-custom-btn',
            textArea: '#id_new_comment',
            owner: '#pullrequest-author li',
            mergeButton: '#fulfill-pullrequest',
            menuHUB: '.odm-hub-header',
            filesAdded: '#commit-files-summary .file-added a',
            infoCommitTab: '#pr-tab-content',
            autosJnk: '[id^=markdown-header-][id*=main]',
        },
        classes: {
            auiButton: '.aui-button',
            auiButtonPrimary: '.aui-button-primary',
            jsCommentButton: '.js-comment-button',
        },
        filesAnalized: false,
        featuresAdded: [],
        $mergeStatus: '',
        $versionStatus: '',
        $reviewerList: '',
        $validations: '',
        $menuValidations: '',
        $upstreamCIStatus: '',
        validations: Odimergie.validations,
        addPullRequestListener: function() {
            $(this.locator.mergeButton).on('click', function() {
                let pod = Odimergie.Collected.project;
                let tickets = Odimergie.Collected.tickets;
                let user = Odimergie.Collected.owner.name;
                let forkName = Odimergie.Collected.forkName;
                let rulesPassed = Odimergie.Collected.dataValidation.passed;
                let rulesFailed = Odimergie.Collected.dataValidation.failed;
                let rulesWarning = Odimergie.Collected.dataValidation.warning;
                let prod = Odimergie.Collected.fixVersion;
                Odimergie.analytics.trackingMerge(pod, tickets, user, forkName, rulesPassed, rulesFailed, rulesWarning, prod);
            });
        },
        addListeners: function() {
            $(document).on('click', '#odm_refresh_button', () => {
                this.privateRender();
            });
        },
        // TODO: Refactor this for do it more friendly **/
        isFeature: function(file) {
            return this.extractExtension(file).toString() === 'feature';
        },
        extractExtension: function(file) {
            return _.last(file.split('.'));
        },
        extractFeatureAndFolder: function(pathFile) {
            return _.last(pathFile.split('/features/'));
        },
        getFeaturesAdded: function() {
            let that = this;
            return _.chain($(this.locator.filesAdded))
                .filter(function(file) {
                    that.filesAnalized = true;
                    return that.isFeature($(file).text().trim());
                })
                .map(function(file) {
                    return that.extractFeatureAndFolder($(file).text().trim());
                })
                .value();
        },
        getTypePullRequestFromDOM: function() {
            let url = window.location.pathname;
            if (Odimergie.regExp.forks.upstream.test(url)) {
                this.pullRequestType = 'upstream';
            } else if (Odimergie.regExp.forks.prod.test(url)) {
                this.pullRequestType = 'prod';
            } else if (Odimergie.regExp.forks.private.test(url)) {
                this.pullRequestType = 'private';
            } else if (url.indexOf(Odimergie.regExp.forks.odf) > -1) {
                this.pullRequestType = 'odf';
            }
        },
        getTitleFromDOM: function() {
            let title = $(this.locator.title).html();
            title = title ? title.trim() : 'Default title';
            Odimergie.Collected.title = title;
        },
        getProjectFromDOM: function() {
            let tickets = Odimergie.regExp.extractTicketCodeList(Odimergie.Collected.title);
            let tempProject = tickets[0];
            Odimergie.Collected.tickets = tickets;
            Odimergie.Collected.project = tempProject ? tempProject.split('-')[0] : tempProject;
        },
        checkOwnerStatus: function(self) {
            if ($(self.locator.owner).data('username')) {
                self.ownerResolve();
                return;
            }
            return setTimeout(function() {
                self.checkOwnerStatus(self);
            }, Odimergie.util.getSeconds(5));
        },
        getOwnerPromise: function() {
            let that = this;
            return new Promise(function(resolve) {
                that.ownerResolve = resolve;
                that.checkOwnerStatus(that);
            });
        },
        getOwnerFromDOM: function() {
            let that = this;
            Odimergie.mentors.getMentorsPromise().then(function() {
                that.getOwnerPromise().then(function() {
                    Odimergie.Collected.owner.name = $(that.locator.owner).data('username');
                    Odimergie.Collected.owner.isMentor = _.contains(_.pluck(Odimergie.mentors.getMentors(), 'userName'),
                        Odimergie.Collected.owner.name);
                    Odimergie.Collected.owner.hasMentor = _.contains(_.pluck(Odimergie.mentors.getMentors(), 'project'), Odimergie.Collected.project);
                });
            });
        },
        getForkNameFromDOM: function() {
            Odimergie.Collected.forkName = $(this.locator.forkName).html().trim();
        },
        getSettingsStatusFromServices: function() {
            return new Promise(function(resolve) {
                Odimergie.services.getSettingsAutomerger(resolve);
            });
        },
        getUpstreamCIStatusFromServices: function() {
            return new Promise(function(resolve) {
                Odimergie.services.getUpstreamStatus(function(status) {
                    resolve(status);
                });
            });
        },
        getCiSynchro: function() {
            const fork = Odimergie.Collected.forkName;
            const pullRequestType = this.pullRequestType;
            return new Promise(function(resolve) {
                if (fork) {
                    const url = (fork === 'odf') ? Odimergie.services.getODFJenkinsUrl() :
                        Odimergie.services.getCIJenkinsURL(fork, pullRequestType === 'upstream');
                    Odimergie.services.getCILastBuildInfo(url, function(buildInfo) {
                        resolve(buildInfo);
                    });
                } else {
                    resolve(null);
                }
            });
        },
        getAutosSynchro: function() {
            const pullRequestType = this.pullRequestType;
            return new Promise(function(resolve) {
                const autosUrl = Odimergie.util.extractAutosLink($(pullRequest.locator.autosJnk));
                if (autosUrl && pullRequestType === 'upstream') {
                    Odimergie.services.getAutosExecutionTime(autosUrl, function(autosTime) {
                        resolve(autosTime);
                    });
                } else {
                    resolve(null);
                }
            });
        },

        getCIStatusFromServices: function() {
            let forkName = this.pullRequestType === 'odf' ? 'odf' : Odimergie.Collected.forkName;
            Odimergie.services.getCIstatus(forkName, false, function(status) {
                Odimergie.Collected.CI.status = status;
            }, this);
        },
        getUpstreamCIStatus: function() {
            let that = this;

            this.getUpstreamCIStatusFromServices().then(function(status) {
                Odimergie.Collected.upstreamCIstatus = status;
                that.$upstreamCIStatus = that.renderUpstreamCIStatus(status);
            });
        },
        getReviewersFromDOM: function() {
            let reviewers = [];
            let dataReviewer = {};
            let $reviewer;
            let localApproves = 0;
            let who = [];
            _.forEach($(this.locator.reviewers), function(reviewer) {
                $reviewer = $(reviewer);
                if ($reviewer.data('username')) {
                    dataReviewer = {
                        'userName': $reviewer.data('username'),
                        'approve': $reviewer.find('a').attr('class').indexOf('approve') > -1,
                        'avatar': $reviewer.find('img').attr('src'),
                    };
                    if (dataReviewer.approve) {
                        localApproves++;
                        who.push(dataReviewer.userName);
                    }
                    if (!pullRequest.isExcluded(dataReviewer.userName) || dataReviewer.approve) {
                        reviewers.push(dataReviewer);
                    }
                }
            }, this);
            Odimergie.Collected.reviewers = reviewers;
            Odimergie.Collected.approves.quantity = localApproves;
            Odimergie.Collected.approves.who = who;
        },
        getConflictsFromDOM: function() {
            Odimergie.Collected.conflicts = $(this.locator.conflicts).length > 0;
        },
        getStaticData: function() {
            let that = this;
            let prom = this.getSettingsStatusFromServices().then(function() {
                Odimergie.Collected.versionStatus = Odimergie.util.getVersionStatus(Odimergie.constants.VERSION, Odimergie.Collected.odiVersion);
                that.$mergeStatus = that.renderMergeStatus(Odimergie.Collected.mergeEnabled);
                that.$versionStatus = that.renderVersionStatus();
            });
            this.getTitleFromDOM();
            this.getProjectFromDOM();
            this.getTypePullRequestFromDOM();
            this.getForkNameFromDOM();
            this.getCIStatusFromServices();
            this.getUpstreamCIStatus();

            Odimergie.services.getAutosMainIdFromDom($(this.locator.autos));
            Odimergie.services.getAutosParallelIdFromDom($(this.locator.autos));
            Odimergie.services.getAutosSingleIdFromDom($(this.locator.autos));
            this.getOwnerFromDOM();
            $(this.locator.infoCommitTab).on('DOMSubtreeModified', function() {
                that.featuresAdded = that.getFeaturesAdded();
                if (that.filesAnalized) {
                    $(that.locator.infoCommitTab).off('DOMSubtreeModified');
                }
            });
            return prom;
        },
        renderReviewerList: function() {
            let html = $('<div></div>');
            _.forEach(Odimergie.Collected.reviewers, function(elementReviewer) {
                html.append(pullRequest.reviewer(elementReviewer));
            }, this);
            return html;
        },
        reviewer: function(element) {
            return $('<div></div>').addClass('odm-inline-block')
                .append($('<img/>', {
                    src: element.avatar,
                    alt: element.userName,
                }).addClass('odm-reviewer-' + (element.approve ? 'approve' : 'no-approve') + ' odm-inline-block'));
        },
        init: function() {
            this.getReviewersFromDOM();
            this.getConflictsFromDOM();
            if (this.pullRequestType === 'upstream') {
                this.addPullRequestListener();
            }
        },
        isExcluded: function(name) {
            return _.some(Odimergie.constants.EXCLUDE_REVIEWER_LIST, function(exclude) {
                return name === exclude;
            });
        },
        initDataValidations: function() {
            Odimergie.Collected.dataValidation.failed = 0;
            Odimergie.Collected.dataValidation.passed = 0;
            Odimergie.Collected.dataValidation.warning = 0;
        },
        checkIsCiAutosSync(ciInfo, autosTime) {
            const commit = $(pullRequest.locator.commit);
            const successMerge = $(pullRequest.locator.successMerge);

            if ((!successMerge || !successMerge[0]) && ciInfo && Odimergie.util.compareCommits(commit, ciInfo)) {
                Odimergie.Collected.isCiSynchro = true;
                Odimergie.Collected.isCiAutosSynchro = autosTime > ciInfo.timestamp + ciInfo.duration - 120000;
            } else if (ciInfo && !ciInfo.commitId || successMerge && successMerge[0]) {
                Odimergie.validations.CI_SYNCHRO.blocker = false;
            }
        },

        renderListWithStyleValidations: function(validations) {
            let html = $('<ul></ul>').append(Odimergie.constants.CMS.TEXT.RULES).addClass('odm-rule-list');
            this.initDataValidations();
            Odimergie.Collected.readyForMerge = true;
            _.each(validations, function(validation) {
                let rule = 'passed';
                if (!validation.execute()) {
                    rule = 'warning';
                    if (validation.blocker) {
                        rule = 'failed';
                        Odimergie.Collected.readyForMerge = false;
                    }
                }
                Odimergie.Collected.dataValidation[rule]++;
                html
                    .append($('<li></li>')
                        .append(validation.title)
                        .addClass('odm-rule-' + rule + ' ' + 'odm-rule-element'));
            }, this);
            return html;
        },
        listenCommentForm: function() {
            let locators = this.locator;
            let attachGutterButtons = _.bind(function(event) {
                pullRequest.attachButtons($(event.currentTarget).parent().parent());
            }, this);
            let attachReplyButtons = _.bind(function(event) {
                pullRequest.attachButtons($(event.currentTarget).closest(locators.comment));
            }, this);

            $(locators.gutter).off('.od-automerge');
            $(locators.editComment).off('.od-automerge');
            $(locators.gutter).on('click.od-automerge', $.proxy(attachGutterButtons, this));
            $(locators.editComment).on('click.od-automerge', $.proxy(attachReplyButtons, this));
            this.attachButtons(locators.generalComments);
        },
        attachButtons: function(place) {
            setTimeout($.proxy(function() {
                let locators = pullRequest.locator;
                let submitButton = $(place).find(locators.submitButton);
                let buttonBox = $(submitButton).parent();
                let buttonColors = ['red', 'orange', 'green'];
                let buttons;

                if (!pullRequest.haveBeenTheBtnsRendered(buttonBox)) {
                    buttons = _.map(buttonColors, function(color) {
                        return pullRequest.renderCommentButton(color);
                    }, pullRequest);

                    pullRequest.hideElement(submitButton);
                    pullRequest.prependCommentButtons(buttonBox, buttons);
                    pullRequest.bindToSubmitButton(buttons);
                }
            }, this), 100);
        },
        haveBeenTheBtnsRendered: function(btnBox) {
            return $(btnBox).find(this.locator.customLocator).length !== 0;
        },
        renderCommentButton: function(color) {
            let $btn = $('<button></button>').html(Odimergie.constants.CMS.TEXT.COMMENT);

            this.addBtnClasses($btn);
            this.addBtnStyle($btn, color);
            this.setBtnType($btn, color);

            return $btn;
        },
        addBtnClasses: function($btn) {
            let classStripper = function(str) {
                return str.slice(1, str.length);
            };
            let locators = this.locator;
            let classes = this.classes;
            let customLocator = classStripper(locators.customLocator);
            let auiButton = classStripper(classes.auiButton);
            let auiButtonPrimary = classStripper(classes.auiButtonPrimary);
            let jsCommentButton = classStripper(classes.jsCommentButton);

            $btn.addClass(auiButton + ' ' + auiButtonPrimary + ' ' + jsCommentButton + ' ' + customLocator);
        },
        addBtnStyle: function($btn, color) {
            $btn.css({
                'background-color': Odimergie.constants.CSS.BUTTON.normal[color],
            });

            $btn.mouseenter(function(e) {
                $(e.target).css({
                    'background-color': Odimergie.constants.CSS.BUTTON.contrast[color],
                });
            });

            $btn.mouseleave(function(e) {
                $(e.target).css({
                    'background-color': Odimergie.constants.CSS.BUTTON.normal[color],
                });
            });
        },
        setBtnType: function($btn, type) {
            $btn.data('type', type);
        },
        hideElement: function(element) {
            $(element).css({
                display: 'none',
            });
        },
        prependCommentButtons: function(selector, buttons) {
            _.each(buttons, function(button) {
                $(selector).prepend(button);
            });
        },
        bindToSubmitButton: function(buttons) {
            _.each(buttons, function(button) {
                button.on('click', _.bind(pullRequest.submitBehavior, pullRequest));
            }, this);
        },
        submitBehavior: function(event) {
            let btnBox = $(event.target).parent();
            let submitBtn = btnBox.find(this.locator.submitButton);
            let btnType = $(event.target).data('type');
            let customFlags = Odimergie.constants.CMS.COMMENT_FLAGS;
            let commentArea = btnBox.parent().find(this.locator.textArea);
            let currentComment = commentArea.val();

            event.preventDefault();
            commentArea.val(customFlags[btnType] + ' ' + currentComment);
            submitBtn.trigger('click');
        },
        disableButton: function(button) {
            if (this.pullRequestType === 'upstream' ||
                this.pullRequestType === 'odf') {
                $(button).addClass('odm-hidden');
            }
        },
        enableButton: function(button) {
            $(button).removeClass('odm-hidden');
        },
        storeReviewerList: function() {
            this.$reviewerList = this.renderReviewerList();
        },
        storeValidations: function() {
            this.$validations = this.renderValidations();
        },
        controlMainConditions: function() {
            let odi = Odimergie.Collected;
            return this.pullRequestType === 'private' || (odi.mergeEnabled && odi.upstreamCIstatus);
        },
        controlMergeButton: function() {
            let odi = Odimergie.Collected;
            if (
                (odi.readyForMerge &&
                    this.controlMainConditions() &&
                    odi.versionStatus !== 'OUTDATED') ||
                Odimergie.debugMode.godMode
            ) {
                this.enableButton(this.locator.mergeButton);
                // TODO instead of setting a timeout, swap the button and refresh it before requesting the pullRequest
                setTimeout(() => {
                    this.disableButton(this.locator.mergeButton);
                }, 1000*60*2);
            } else {
                this.disableButton(this.locator.mergeButton);
            }
        },
        synchroJenkinsInfo: function() {
            return Promise.all([
                this.getCiSynchro(),
                this.getAutosSynchro(),
            ]);
        },
        refreshOptions: function() {
            this.listenCommentForm();
            this.storeReviewerList();
            this.storeValidations();
            return Promise.all([
                this.getUpstreamCIStatus(),
                this.getSettingsStatusFromServices(),
                this.controlMergeButton(),
            ]);
        },
        renderSingleTime: function() {
            this.init();
            this.listenCommentForm();
            this.getSettingsStatusFromServices();
            this.storeReviewerList();
            this.storeValidations();
            this.controlMergeButton();
        },
        getValidationsByType: function(type) {
            return _.filter(this.validations, function(validation) {
                return _.isObject(validation.enable) && validation.enable[type];
            });
        },
        renderValidations: function() {
            return this.renderListWithStyleValidations(this.getValidationsByType(this.pullRequestType));
        },
        renderStatus: function(status, text) {
            return $('<div></div>').addClass(Odimergie.util.getCssColorStatus(status.toString())).append(text);
        },
        renderMergeStatus: function(status) {
            let text = status !== 'FALSE' ? Odimergie.constants.CMS.TEXT.MERGE.ENABLED : Odimergie.constants.CMS.TEXT.MERGE.DISABLED;
            return this.renderStatus(status, text);
        },
        renderVersionStatus: function() {
            let status = Odimergie.Collected.versionStatus;
            let text = Odimergie.constants.CMS.TEXT.VERSION[Odimergie.util.getTextStatus(status)];
            return this.renderStatus(status, text);
        },
        renderUpstreamCIStatus: function(status) {
            return this.renderStatus(status, Odimergie.constants.CMS.TEXT.CI.UPSTREAM[Odimergie.util.getTextStatus(status)]);
        },
        getNewPullRequestClass: function(type) {
            let map = {
                prod: 'odm-merge-button-prod',
                private: 'odm-merge-button-private',
                upstream: 'odm-merge-button-upstream',
                odf: 'odm-merge-button-odf',
            };
            return map[type] || 'odm-merge-button-default';
        },
        setNameMergeButton: function() {
            let type = this.pullRequestType;
            let colorClass = this.getNewPullRequestClass(type);
            $('#fulfill-pullrequest').addClass(colorClass).html(type ? type.toUpperCase() : 'Merge');
        },
        appendMergeStatus: function() {
            return this.pullRequestType === 'private' ? '' : pullRequest.$mergeStatus;
        },
        appendUpstreamCIStatus: function() {
            return this.pullRequestType === 'private' ? '' : pullRequest.$upstreamCIStatus;
        },
        renderInfo: function() {
            Odimergie.HUB.render()
                .append($('<div></div>', {
                    id: 'HUB-content-' + Odimergie.constants.CMS.PAGENAME.PULLREQUEST,
                }).addClass('HUB-content')
                    .append($('<div></div>', {
                        id: 'odm_global_rules',
                    })
                        .append(this.appendMergeStatus())
                        .append(this.appendUpstreamCIStatus())
                        .append(pullRequest.$versionStatus)
                    )
                    .append(pullRequest.$reviewerList)
                    .append(pullRequest.$validations));
        },
        privateRender: function() {
            this.init();
            this.synchroJenkinsInfo()
                .then((res) => {
                    const ciInfo = res[0];
                    const autosTime = res[1];
                    this.checkIsCiAutosSync(ciInfo, autosTime);
                })
                .then(() => this.refreshOptions())
                .then(() => this.getStaticData())
                .then(() => this.renderInfo());
        },
        render: function() {
            this.addListeners();
            this.setNameMergeButton();
            if (Odimergie.debugMode.refresh) {
                this.renderSingleTime();
            }
            this.privateRender();
        },
    };
    $('#fulfill-pullrequest').addClass('odm-hidden');
    module.exports = pullRequest;
})();

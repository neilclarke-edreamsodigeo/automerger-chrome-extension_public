const $ = require('jquery');
const _ = require('underscore');
const Constants = require('../data/constants');
const Odimergie = {};
Odimergie.storage = require('../util/storage');
Odimergie.util = require('../util/util');
const createPullRequest = {
    localStorageDataPrefix: 'PRData-',
    jiraLink: '',
    jiraTicketId: '',
    forkName: '',
    locators: {
        body: '#pullrequest-form',
        mainParallelId: {
            desktop: '.desktop_id',
            mobile: '.mobile_id',
            main: '.main_id',
        },
        jiraTicketInput: '.jira_ticket_input',
        generateTemplateBtn: '.generate_template',
        savePRDataBtn: '.save_pr_data',
        exportText: '.export_txt',
        exportTextJira: '.export_txt_jira',
        dismissTool: '.dismiss_pr_tool',
        toggleTool: '.toggle_tool',
        removeAllForksData: '.remove_all_forks_data',
        removeThisForkData: '.remove_this_fork_data',
        useOldJenkins: '.use_old_jenkins',
        useNewJenkins: '.use_new_jenkins',
        useAreaJenkins: '.use_area_jenkins',
        jenkinsVersion: '.jenkins_version',
        areaJenkinsSelectorWrapper: '.area_jenkins_selector_wrapper ',
        areaJenkinsSelector: '.area_jenkins_selector',
        commitsLinks: '#commits .subject a',
        prDescription: '#id_description',
        repoName: '#id_source_group .repo-name',
        failedAutosBtns: {
            addJenkinsFailed: '.add_failed_auto_jenkins',
            addKnownIssue: '.add_failed_auto_known',
            addUnknownIssue: '.add_failed_auto_unknown',
            removeFail: '.remove_fail',
        },
        autosFormContainers: '.autos_container',
        failedAutosContainers: '.failed_autos',
        failedAutosTabsLinksContainer: '.failed_autos_tabs_nav',
        failedAutosTabs: '.failed_autos_tab_link',
        failedAutosTabsContentContainer: '.failed_autos_tabs_content_container',
        failedAutosContent: '.failed_autos_tab_content',
        autos: '.failed_auto',
        failedTypeByData: {
            jenkins: '[data-fail-type="0"]',
            knownIssues: '[data-fail-type="1"]',
            unknownIssues: '[data-fail-type="2"]',
        },
        templates: {
            failedForms: ['#failed_auto_jenkins_tmpl', '#failed_auto_known_issue_tmpl', '#failed_auto_unknown_issue_tmpl'],
            desktopFailTabLink: '#failed_auto_tab_link_tmpl',
        },

        bitbucketPreviewBtn: '.preview',
    },
    defaultAutosFailedData: {
        browser: '',
        feature: '',
        seed: '',
        single: '',
        compareWithSeed: '',
        compareWithoutSeed: '',
        comments: '',
        ticket: '',
        tabId: '',
    },
    togglePreviewMode: function() {
        $(this.locators.bitbucketPreviewBtn).click();
    },
    downloadTemplate: function(exportButton, text, name) {
        let file = new window.Blob([text], {type: 'text/plain'});
        $(exportButton).attr('href', window.URL.createObjectURL(file));
        $(exportButton).attr('download', name);
    },
    generateTemplate: function(mainId) {
        let bitBucketMarkdown;
        let jiraMarkDown;
        let prData = this.generatePRData();
        let isInPreviewMode = $(this.locators.bitbucketPreviewBtn).text() === 'Edit';
        let $exportTxtBtn = $(this.locators.exportText);
        let $exportTxtJira = $(this.locators.exportTextJira);

        if (isInPreviewMode) {
            this.togglePreviewMode();
        }

        bitBucketMarkdown = this.generateBitBucketMarkdown(prData, mainId);
        jiraMarkDown = this.generateJiraMarkdown(prData, mainId);

        this.downloadTemplate($exportTxtBtn, bitBucketMarkdown, `${this.jiraTicketId}_template.txt`);
        this.downloadTemplate($exportTxtJira, jiraMarkDown, `${this.jiraTicketId}_jira_template.txt`);

        $(this.locators.prDescription).val(bitBucketMarkdown);

        this.togglePreviewMode();
    },
    itsBitBucket: function(platform) {
        return platform === Constants.CMS.BITBUCKET;
    },
    generateBitBucketMarkdown: function(prData, mainId) {
        let bitbucket = Constants.CMS.BITBUCKET;
        let markdown = `### ${Constants.CMS.JENKINSVERSION}: ${prData.jenkinsVersion} ${Constants.CMS.BREAK_LINE}`;

        markdown += this.renderJiraLink() + Constants.CMS.BREAK_LINE;
        markdown += this.renderCIState(bitbucket) + Constants.CMS.BREAK_LINE;
        markdown += this.renderAutosTitle(bitbucket);
        markdown += this.renderAutosState(mainId, 'Main', prData.main, bitbucket) + Constants.CMS.BREAK_LINE;

        return markdown;
    },
    generateJiraMarkdown: function(prData, mainId) {
        let jira = Constants.CMS.JIRA;
        let markdown = this.renderCIState(jira) + Constants.CMS.BREAK_LINE;

        markdown += this.renderAutosTitle(jira);
        markdown += this.renderAutosState(mainId, 'Main', prData.main, jira) + Constants.CMS.BREAK_LINE;

        return markdown;
    },
    generatePRData: function() {
        if (!this.jiraLink) {
            this.jiraLink = $(this.locators.jiraTicketInput).val();
        }
        return {
            'jiraLink': this.jiraLink,
            'jenkinsVersion': Odimergie.util.getJenkinsVersion(),
            'main': {
                'autoId': this.getAutoId(this.locators.mainParallelId.main),
                'jenkinsFails': this.getJenkinsFailedAutos('main'),
                'knownIssueFails': this.getKnownIssueFailedAutos('main'),
                'unknownIssueFails': this.getUnknownIssueFailedAutos('main'),
            },
        };
    },
    savePRData: function() {
        window.localStorage.setItem(this.localStorageDataPrefix + this.forkName, JSON.stringify(this.generatePRData()));
    },
    dismissTool: function(showTool) {
        window.localStorage.setItem('PR_TEMPLATE_GENERATOR_TOOL_DISMISS', showTool);
        window.location.reload();
    },
    toggleTool: function() {
        $('.toggle_open_icon').toggleClass('odm-hidden');
        $('.toggle_close_icon').toggleClass('odm-hidden');
        $('.tool_main_container').toggleClass('show');
    },
    removeThisForkData: function() {
        let storedData = JSON.parse(window.localStorage.getItem(this.localStorageDataPrefix + this.forkName));
        let sureRemove;

        if (storedData) {
            sureRemove = window.confirm(Constants.CMS.DATA_REMOVE_DISCLAIMER_THIS);

            if (sureRemove) {
                window.localStorage.removeItem(this.localStorageDataPrefix + this.forkName);
                window.location.reload();
            }
        } else {
            window.alert(Constants.CMS.NO_DATA_FOUND_THIS);
        }
    },
    removeAllForksData: function() {
        let forksData = this.findLocalItems(this.localStorageDataPrefix);
        let i;
        let z = forksData.length;
        let sureRemove = window.confirm(Constants.CMS.DATA_REMOVE_DISCLAIMER_ALL);

        if (forksData) {
            if (sureRemove) {
                for (i = 0; i < z; i++) {
                    window.localStorage.removeItem(forksData[i].key);
                }
                window.location.reload();
            }
        } else {
            window.alert(Constants.CMS.NO_DATA_FOUND_ALL);
        }
    },
    setJenkinsSeleted: function(jenkinsType, isAreaJenkins) {
        Odimergie.util.setJenkinsType(jenkinsType);
        $(this.locators.areaJenkinsSelectorWrapper).toggleClass('hidden', !isAreaJenkins);
        if (isAreaJenkins) {
            const $selector = $(this.locators.areaJenkinsSelector);
            if (!$selector.hasClass('filled')) {
                $selector.off();
                _.forEach(Constants.AREAS, (area) => {
                    $('<option value="' + area.port + '">' + area.name + '</option>').appendTo($selector);
                });
                $selector.addClass('filled').on('change', this.setAreaJenkinsSelected);
            }
        }
        $(this.locators.jenkinsVersion).html(Odimergie.util.getJenkinsVersion());
    },

    setAreaJenkinsSelected: function() {
        window.localStorage.setItem('areaSelected', $(this).val());
    },

    getJenkinsFailedAutos: function(device) {
        let $autosContainer = this.getAutosContainer(device);
        let $jenkinsFails = $autosContainer.find(this.locators.failedTypeByData.jenkins);
        let failedAutosData = [];
        let i;
        let z = $jenkinsFails.length;
        let autoBrowser;

        for (i = 0; i < z; i++) {
            autoBrowser = $jenkinsFails.eq(i).find('.auto_browser').val();

            failedAutosData.push({
                type: $jenkinsFails.eq(i).data('failType'),
                browser: autoBrowser ? autoBrowser : $jenkinsFails.eq(i).find('.auto_browser_input').val(),
                feature: $jenkinsFails.eq(i).find('.auto_feature').val(),
                seed: $jenkinsFails.eq(i).find('.auto_seed').val(),
                single: $jenkinsFails.eq(i).find('.auto_single').val().trim(),
                tabId: this.getTabId($jenkinsFails.eq(i)),
                comments: $jenkinsFails.eq(i).find('.auto_desc_field').val(),
            });
        }
        return failedAutosData;
    },
    getKnownIssueFailedAutos: function(device) {
        let $autosContainer = this.getAutosContainer(device);
        let $knownIssues = $autosContainer.find(this.locators.failedTypeByData.knownIssues);
        let failedAutosData = [];
        let i;
        let z = $knownIssues.length;
        let autoBrowser;

        for (i = 0; i < z; i++) {
            autoBrowser = $knownIssues.eq(i).find('.auto_browser').val();

            failedAutosData.push({
                type: $knownIssues.eq(i).data('failType'),
                browser: autoBrowser ? autoBrowser : $knownIssues.eq(i).find('.auto_browser_input').val(),
                feature: $knownIssues.eq(i).find('.auto_feature').val(),
                ticket: $knownIssues.eq(i).find('.auto_ticket').val(),
                tabId: this.getTabId($knownIssues.eq(i)),
                comments: $knownIssues.eq(i).find('.auto_desc_field').val(),
            });
        }
        return failedAutosData;
    },
    getUnknownIssueFailedAutos: function(device) {
        let $autosContainer = this.getAutosContainer(device);
        let $unknownIssues = $autosContainer.find(this.locators.failedTypeByData.unknownIssues);
        let failedAutosData = [];
        let i;
        let z = $unknownIssues.length;
        let autoBrowser;

        for (i = 0; i < z; i++) {
            autoBrowser = $unknownIssues.eq(i).find('.auto_browser').val();

            failedAutosData.push({
                type: $unknownIssues.eq(i).data('failType'),
                browser: autoBrowser ? autoBrowser : $unknownIssues.eq(i).find('.auto_browser_input').val(),
                feature: $unknownIssues.eq(i).find('.auto_feature').val(),
                seed: $unknownIssues.eq(i).find('.auto_seed').val(),
                compareWithSeed: $unknownIssues.eq(i).find('.compare_with_seed').val().trim(),
                compareWithoutSeed: $unknownIssues.eq(i).find('.compare_without_seed').val().trim(),
                tabId: this.getTabId($unknownIssues.eq(i)),
                comments: $unknownIssues.eq(i).find('.auto_desc_field').val(),
            });
        }
        return failedAutosData;
    },
    findLocalItems: function(query) {
        let prData;
        let results = [];
        let value;
        let localStorage = window.localStorage;

        for (prData in localStorage) {
            if (localStorage.hasOwnProperty(prData) && (prData.match(query) || (!query && typeof prData === 'string'))) {
                value = JSON.parse(localStorage.getItem(prData));
                results.push({key: prData, val: value});
            }
        }
        return results;
    },
    generateTabId: function() {
        let text = '';
        let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let i;

        for (i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    },
    getAutoId: function(input) {
        return $(input).val().trim();
    },

    getJiraLink: function(data) {
        let jiraLink = '';

        if (this.jiraTicketId) {
            jiraLink = `${Constants.JIRA}browse/${this.jiraTicketId}`;
        } else if (data && data.jiraLink) {
            jiraLink = data.jiraLink;
        }

        return jiraLink;
    },

    getJiraTicketId: function() {
        let jiraTicketIdRegexp = /([a-z]|[A-Z])+(-)([0-9]+)/g;
        let jiraTicket = jiraTicketIdRegexp.exec(this.forkName);

        return jiraTicket ? jiraTicket[0] : '';
    },
    getForkNameFromDom: function() {
        return $(this.locators.repoName)[0].text;
    },

    // Handlers for FAILED AUTOS buttons //
    removeFailedAuto: function(ev) {
        let $autosContainer = $(ev.target).closest(this.locators.autosFormContainers);
        let autoId = this.getTabId($(ev.target));
        let nextActiveTabId;

        $(this.locators.failedAutosTabs + '[data-tab-content-id="' + autoId + '"]').remove();
        $(`#${autoId}`).parent().remove();

        nextActiveTabId = this.getTabContentId($autosContainer);
        this.activateTabContent(nextActiveTabId);
        this.activateTabLink(nextActiveTabId);
    },
    addFailedAuto: function(ev) {
        let failType = $(ev.target).data('failTypeButton'); // 0, 1 or 2
        let $autosContainer = $(ev.target).closest(this.locators.autosFormContainers);
        let $tabLinksContainer = $autosContainer.find(this.locators.failedAutosTabsLinksContainer);
        let $tabLinks = $autosContainer.find(this.locators.failedAutosTabs);
        let $failedAutosTabContent = $autosContainer.find(this.locators.failedAutosContent);
        let tmpl = _.template($(this.locators.templates.failedForms[failType]).html());
        let tmplTabLink = _.template($(this.locators.templates.desktopFailTabLink).html());
        let tabId = this.generateTabId();

        $failedAutosTabContent.removeClass('active');
        $tabLinks.removeClass('active');

        $tabLinksContainer.append(tmpl(_.extend({renderGroupsOptions: this.renderGroupsOptions}, this.defaultAutosFailedData, {
            tabId: tabId,
        })));
        $tabLinksContainer.prepend(tmplTabLink({
            tabId: tabId,
            tabText: Constants.CMS.AUTOS_ICONS[failType],
        }));

        this.activateTabContent(tabId);
        this.activateTabLink(tabId);
    },
    changeDesktopActiveTab: function(ev) {
        let tabSelectedId = $(ev.target).data('tabContentId');
        let $autosContainer = $(ev.target).closest(this.locators.autosFormContainers);
        let $failedAutosTabContent = $autosContainer.find(this.locators.failedAutosContent);
        let $tabLinks = $autosContainer.find(this.locators.failedAutosTabs);

        $failedAutosTabContent.removeClass('active');
        $tabLinks.removeClass('active');
        this.activateTabContent(tabSelectedId);
        this.activateTabLink(tabSelectedId);
    },
    activateTabContent: function(tabId) {
        $(`#${tabId}`).addClass('active');
    },
    activateTabLink: function(tabId) {
        $(`[data-tab-content-id="${tabId}"]`).addClass('active');
    },

    getAutosContainer: function(device) {
        return $(`[data-device="${device}"]`);
    },
    getTabId: function(tab) {
        return tab.closest(this.locators.autos).find(this.locators.failedAutosContent).attr('id');
    },
    getTabContentId: function(autosContainer) {
        return autosContainer.find(this.locators.failedAutosTabs).first().data('tabContentId');
    },
    renderFailedAutos: function(storedData, device) {
        $(this.locators.mainParallelId[device]).val(storedData[device].autoId);

        if (storedData[device].jenkinsFails) {
            this.renderFailTabs(storedData[device].jenkinsFails, device);
        }

        if (storedData[device].knownIssueFails) {
            this.renderFailTabs(storedData[device].knownIssueFails, device);
        }

        if (storedData[device].unknownIssueFails) {
            this.renderFailTabs(storedData[device].unknownIssueFails, device);
        }
    },
    renderGroupValues: function(device, data) {
        let jenkinsFails = data.jenkinsFails;
        let knowunIssueFails = data.knownIssueFails;
        let unknownIssueFails = data.unknownIssueFails;

        this.renderBrowserInputValue(device, jenkinsFails);
        this.renderBrowserInputValue(device, knowunIssueFails);
        this.renderBrowserInputValue(device, unknownIssueFails);
    },
    renderBrowserInputValue: function(device, data) {
        let $autosContainer = this.getAutosContainer(device);
        let $tabContent;
        let autoBrowserSelect;
        let i;
        let z = data.length;

        for (i = 0; i < z; i++) {
            $tabContent = $autosContainer.find(`#${data[i].tabId}`);
            autoBrowserSelect = $tabContent.find('.auto_browser');
            if (!autoBrowserSelect.val()) {
                $tabContent.find('.auto_browser_input').val(data[i].browser);
            }
        }
    },
    init: function() {
        let storedData;

        this.forkName = this.getForkNameFromDom();
        this.jiraTicketId = this.getJiraTicketId();
        storedData = this.getLocalStoragePRData();
        this.jiraLink = this.getJiraLink(storedData);

        $(this.locators.body).prepend(this.renderTemplateGenerator(this.jiraLink));
        this.renderStoredData(storedData);
        this.setJenkinsSeleted('AREA', true);
        this.bindEvents();
    },
    getLocalStoragePRData: function() {
        return JSON.parse(window.localStorage.getItem(this.localStorageDataPrefix + this.forkName));
    },
    renderStoredData: function(data) {
        let $mainAutosContainer = this.getAutosContainer('main');
        let mainActiveTabId;

        if (data) {
            this.renderFailedAutos(data, 'main');
            mainActiveTabId = this.getTabContentId($mainAutosContainer);
            this.activateTabContent(mainActiveTabId);
            this.activateTabLink(mainActiveTabId);
        }
    },
    bindEvents: function() {
        $(this.locators.generateTemplateBtn).on('click', $.proxy(this, 'onGenerateTemplateClick'));
        $(this.locators.savePRDataBtn).on('click', $.proxy(this, 'savePRData'));
        $(this.locators.removeThisForkData).on('click', $.proxy(this, 'removeThisForkData'));
        $(this.locators.removeAllForksData).on('click', $.proxy(this, 'removeAllForksData'));
        $(this.locators.useNewJenkins).on('click', $.proxy(this, 'setJenkinsSeleted', 'NEW', false));
        $(this.locators.useOldJenkins).on('click', $.proxy(this, 'setJenkinsSeleted', 'OLD', false));
        $(this.locators.useAreaJenkins).on('click', $.proxy(this, 'setJenkinsSeleted', 'AREA', true));
        $(this.locators.toggleTool).on('click', $.proxy(this, 'toggleTool'));

        // Manage failed autos buttons listeners
        $(this.locators.autosFormContainers).on('click', this.locators.failedAutosBtns.removeFail, $.proxy(this, 'removeFailedAuto'));
        $(this.locators.failedAutosBtns.addJenkinsFailed).on('click', $.proxy(this, 'addFailedAuto'));
        $(this.locators.failedAutosBtns.addKnownIssue).on('click', $.proxy(this, 'addFailedAuto'));
        $(this.locators.failedAutosBtns.addUnknownIssue).on('click', $.proxy(this, 'addFailedAuto'));
        $(this.locators.failedAutosTabsLinksContainer).on('click', this.locators.failedAutosTabs, $.proxy(this, 'changeDesktopActiveTab'));
    },
    renderFailTabs: function(fails, device) {
        let failType;
        let tmpl;
        let tmplTabLink;
        let i;
        let z = fails.length;

        if (z) {
            failType = _.first(fails) ? _.first(fails).type : 0;
            tmpl = _.template($(this.locators.templates.failedForms[failType]).html());
            tmplTabLink = _.template($(this.locators.templates.desktopFailTabLink).html());

            for (i = 0; i < z; i++) {
                this.renderTabContent(tmpl, fails[i], device);
                this.renderTabLinks(tmplTabLink, fails[i], device);
            }
        }
    },
    renderTabContent: function(template, fail, device) {
        let $autosContainer = this.getAutosContainer(device);

        $autosContainer.find(this.locators.failedAutosTabsContentContainer)
            .append(template(_.extend({renderGroupsOptions: this.renderGroupsOptions}, this.defaultAutosFailedData, fail, {
                tabId: fail.tabId,
            })));
    },
    renderTabLinks: function(template, fail, device) {
        let $autosContainer = this.getAutosContainer(device);

        $autosContainer.find(this.locators.failedAutosTabsLinksContainer).prepend(template({
            tabId: fail.tabId,
            tabText: Constants.CMS.AUTOS_ICONS[fail.type],
        }));
    },
    onGenerateTemplateClick: function() {
        this.savePRData();
        this.generateTemplate($(this.locators.mainParallelId.main).val());
    },
    renderJiraLink: function() {
        return `**Jira:** ${this.jiraLink}`;
    },
    renderCIState: function(platform) {
        const jenkinsCI = Constants[`JENKINS_${Odimergie.util.getJenkinsVersion()}`].CI;
        const forkName = `${jenkinsCI}${this.forkName}`;
        let bbState = `## **CI** - [![Build Status](${forkName}/badge/icon)](${forkName})`;
        let jiraState = `h2. *CI* - [!${forkName}/badge/icon!|${forkName}]`;

        return this.itsBitBucket(platform) ? bbState : jiraState;
    },
    renderAutosTitle: function(platform) {
        let bbTitle = `## Autos: ${Constants.CMS.BREAK_LINE}`;
        let jiraTitle = `h2. Autos: ${Constants.CMS.BREAK_LINE}`;

        return this.itsBitBucket(platform) ? bbTitle : jiraTitle;
    },
    renderGroupsOptions: function(group) {
        let options = '';
        let groups = Constants[`JENKINS_${Odimergie.util.getJenkinsVersion()}`].GROUPS;
        let selected = '';
        let i;
        let z = groups.length;

        for (i = 0; i < z; i++) {
            selected = groups[i] === group ? 'selected' : '';
            options += `<option value="${groups[i]}" ${selected}> ${groups[i]} </option>`;
        }

        return options;
    },
    renderJenkinsFailMarkdown: function(jenkinsFails, platform) {
        let markdown = this.renderFailTypeTitle(0, platform);
        let i;
        let z = jenkinsFails.length;
        let comments;

        for (i = 0; i < z; i++) {
            comments = jenkinsFails[i].comments;

            markdown += this.renderFailedBrowser(jenkinsFails[i].browser, platform);
            markdown += this.renderFailedFeature(jenkinsFails[i].feature, platform);
            markdown += jenkinsFails[i].single.trim() ? this.renderSingle(jenkinsFails[i].single.trim(), platform) : '';
            markdown += comments ? this.renderComments(comments, platform) : '';
            markdown += `--- ${Constants.CMS.BREAK_LINE}`;
        }

        return markdown;
    },
    renderKnownIssueFailMarkdown: function(knownIssues, platform) {
        let markdown = this.renderFailTypeTitle(1, platform);
        let i;
        let z = knownIssues.length;
        let comments;
        let ticket;

        for (i = 0; i < z; i++) {
            ticket = knownIssues[i].ticket;
            comments = knownIssues[i].comments;
            markdown += this.renderFailedBrowser(knownIssues[i].browser, platform);
            markdown += this.renderFailedFeature(knownIssues[i].feature, platform);
            markdown += ticket ? this.renderTicket(knownIssues[i].ticket) : '';
            markdown += comments ? this.renderComments(comments, platform) : '';
            markdown += `--- ${Constants.CMS.BREAK_LINE}`;
            ticket = '';
        }

        return markdown;
    },
    renderUnknownIssueFailMarkdown: function(unknownIssues, platform) {
        let markdown = this.renderFailTypeTitle(2, platform);
        let compareWithSeed;
        let compareWithoutSeed;
        let i;
        let z = unknownIssues.length;
        let comments;

        for (i = 0; i < z; i++) {
            compareWithSeed = unknownIssues[i].compareWithSeed;
            compareWithoutSeed = unknownIssues[i].compareWithoutSeed;
            comments = unknownIssues[i].comments;

            markdown += this.renderFailedBrowser(unknownIssues[i].browser, platform);
            markdown += this.renderFailedFeature(unknownIssues[i].feature, platform);
            markdown += compareWithSeed ? this.renderCompare(compareWithSeed, platform, true) : '';
            markdown += compareWithoutSeed ? this.renderCompare(compareWithoutSeed, platform, false) : '';
            markdown += comments ? this.renderComments(comments, platform) : '';
            markdown += `--- ${Constants.CMS.BREAK_LINE}`;
        }

        return markdown;
    },
    renderAutosState: function(id, type, failedAutos, platform) {
        let markdown = '';
        let jenkinsFails = failedAutos.jenkinsFails;
        let knownIssues = failedAutos.knownIssueFails;
        let unknownIssues = failedAutos.unknownIssueFails;

        markdown += this.renderAutoMainParallel(type, id, platform);

        if (jenkinsFails.length) {
            markdown += this.renderJenkinsFailMarkdown(jenkinsFails, platform);
        }

        if (knownIssues.length) {
            markdown += this.renderKnownIssueFailMarkdown(knownIssues, platform);
        }

        if (unknownIssues.length) {
            markdown += this.renderUnknownIssueFailMarkdown(unknownIssues, platform);
        }

        return markdown;
    },
    renderAutoMainParallel: function(type, id, platform) {
        const job = Constants[Odimergie.util.isJenkinsType('AREA') ? 'JOBS-AREA' : 'JOBS'].MAIN;
        const portArea = window.localStorage.getItem('areaSelected') || '';
        const jobPath = `${Constants[`JENKINS_${Odimergie.util.getJenkinsVersion()}`].AUTOS}${job}`.replace('#PORT#', portArea);
        const bbState = `### ** ${type} ** - [![Build Status](${jobPath}/${id}/badge/icon)](${jobPath}/${id})${Constants.CMS.BREAK_LINE}`;
        const jiraState = `h3. *${type}* - [!${jobPath}/${id}/badge/icon!|${jobPath}${id}/]`;

        return this.itsBitBucket(platform) ? bbState : jiraState;
    },
    renderFailTypeTitle: function(failType, platform) {
        let bbState = `**${Constants.CMS.AUTOS_FAILTYPES_TITLES[failType]}** ${Constants.CMS.BREAK_LINE}`;
        let jiraState = `*${Constants.CMS.AUTOS_FAILTYPES_TITLES[failType]}* ${Constants.CMS.BREAK_LINE}`;

        return this.itsBitBucket(platform) ? bbState : jiraState;
    },
    renderFailedBrowser: function(browser, platform) {
        let bbState = `**Browser**: ${browser} ${Constants.CMS.BREAK_LINE}`;
        let jiraState = `*Browser*: ${browser} ${Constants.CMS.BREAK_LINE}`;

        return this.itsBitBucket(platform) ? bbState : jiraState;
    },
    renderFailedFeature: function(feature, platform) {
        let bbState = `**Feature**: ${feature} ${Constants.CMS.BREAK_LINE}`;
        let jiraState = `*Feature*: ${feature} ${Constants.CMS.BREAK_LINE}`;

        return this.itsBitBucket(platform) ? bbState : jiraState;
    },
    renderFailedSeed: function(seed) {
        return `**Seed: ** ${seed} ${Constants.CMS.BREAK_LINE}`;
    },
    renderSingle: function(single, platform) {
        const portArea = window.localStorage.getItem('areaSelected') || '';
        const job = Constants[Odimergie.util.isJenkinsType('AREA')? 'JOBS-AREA' : 'JOBS'].SINGLE;
        const jobPath = `${Constants[`JENKINS_${Odimergie.util.getJenkinsVersion()}`].AUTOS}${job}`.replace('#PORT#', portArea);
        const badgePath = `${jobPath}/${single}/badge/icon`;
        const prefixTitle = 'Single ';

        const bbState = `**${prefixTitle}**: [![Build Status](${badgePath})](${jobPath}/${single})${Constants.CMS.BREAK_LINE}`;
        const jiraState = `**${prefixTitle}**: [!${badgePath}!|${jobPath}${single}/]${Constants.CMS.BREAK_LINE}`;

        return this.itsBitBucket(platform) ? bbState : jiraState;
    },
    renderCompare: function(build, platform, seed) {
        const portArea = window.localStorage.getItem('areaSelected') || '';
        const job = Constants[Odimergie.util.isJenkinsType('AREA')? 'JOBS-AREA' : 'JOBS'].COMPARE;
        const jobPath = `${Constants[`JENKINS_${Odimergie.util.getJenkinsVersion()}`].AUTOS}${job}`.replace('#PORT#', portArea);
        const badgePath = `${jobPath}/${build}/badge/icon`;
        const prefixTitle = (seed) ? 'Compare build With Seed ' : 'Compare build Without Seed ';

        const bbState = `**${prefixTitle}**: [![Build Status](${badgePath})](${jobPath}/${build})${Constants.CMS.BREAK_LINE}`;
        const jiraState = `**${prefixTitle}**: [!${badgePath}!|${jobPath}${build}/]${Constants.CMS.BREAK_LINE}`;

        return this.itsBitBucket(platform) ? bbState : jiraState;
    },
    renderComments: function(comments, platform) {
        let bbComments = `**Comments:** ${comments}`;
        let jiraComments = `*Comments:* ${comments}`;

        return (this.itsBitBucket(platform) ? bbComments : jiraComments) + Constants.CMS.BREAK_LINE;
    },
    renderParallelUpstream: function(parallel, platform) {
        const job = Constants[Odimergie.util.isJenkinsType('AREA') ? 'JOBS-AREA' : 'JOBS'].PARALLEL;
        const portArea = window.localStorage.getItem('areaSelected') || '';
        const jobPath = `${Constants[`JENKINS_${Odimergie.util.getJenkinsVersion()}`].AUTOS}${job}`.replace('#PORT#', portArea);
        const badgePath = `${jobPath}/${parallel}/badge/icon`;
        const bbState = `**Parallel over upstream**: [![Build Status](${badgePath})](${jobPath}/${parallel})${Constants.CMS.BREAK_LINE}`;
        const jiraState = `*Parallel over upstream*: [!${badgePath}!|${jobPath}/${parallel}']${Constants.CMS.BREAK_LINE}`;

        return this.itsBitBucket(platform) ? bbState : jiraState;
    },
    renderParallelFork: function(parallel, platform) {
        const job = Constants[Odimergie.util.isJenkinsType('AREA') ? 'JOBS-AREA' : 'JOBS'].PARALLEL_ARTIFACT;
        const portArea = window.localStorage.getItem('areaSelected') || '';
        const jobPath = `${Constants[`JENKINS_${Odimergie.util.getJenkinsVersion()}`].AUTOS}${job}`.replace('#PORT#', portArea);
        const badgePath = `${jobPath}/${parallel}/badge/icon`;
        const bbState = `**Parallel over my fork ** - [![Build Status](${badgePath})](${jobPath}/${parallel})${Constants.CMS.BREAK_LINE}`;
        const jiraState = `* Parallel over my fork * - [!${badgePath}!|${jobPath}${parallel}/]`;

        return this.itsBitBucket(platform) ? bbState : jiraState;
    },
    renderTicket: function(ticket) {
        return `**Ticket**: ${Constants.JIRA}browse/${ticket}${Constants.CMS.BREAK_LINE}`;
    },
    renderTemplateGenerator: require('./renderTemplateGenerator'),
    renderShowTool: function() {
        return `<div class="aui-button aui-button-primary odm-button-warning odm-see-pr-tool dismiss_pr_tool"
                            data-show-tool="false" title="SHOW PR TOOL">${Constants.CMS.SEE_TOOL_DISCLAIMER}</div>`;
    },

    bindDismissEvent: function() {
        $(document).on('click', this.locators.dismissTool, _.bind(function(ev) {
            let showTool = $(ev.target).data('showTool');
            createPullRequest.dismissTool(showTool);
        }, this));
    },

    render: function() {
        let isDismissed = JSON.parse(window.localStorage.getItem('PR_TEMPLATE_GENERATOR_TOOL_DISMISS'));

        this.bindDismissEvent();

        if (!isDismissed) {
            this.init();
        } else {
            $(this.locators.body).prepend(this.renderShowTool());
        }
    },
};

module.exports = createPullRequest;

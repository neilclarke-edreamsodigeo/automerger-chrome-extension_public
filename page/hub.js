(function() {
    'use strict';
    const Odimergie = {};
    const $ = require('jquery');
    const _ = require('underscore');
    Odimergie.constants = require('../data/constants');
    Odimergie.storage = require('../util/storage');
    Odimergie.Collected = Odimergie.Collected || require('../data/collected');
    Odimergie.util = require('../util/util');
    Odimergie.services = require('../data/services');
    Odimergie.settingsMenu = require('../page/settingsMenu');
    const jenkinsVersion = Odimergie.util.getJenkinsVersion();
    const hub = {
        id: 'HUB',
        locators: {
            resize: '#odm_resize_button',
            settings: '#odm_settings_button',
            extendedInfo: 'odm_info_overlay',
            fixVersionId: 'odm-fixversion',
        },
        urls: {
            'JIRA': Odimergie.constants.JIRA + 'browse/',
            'UPSTREAM': Odimergie.constants.BITBUCKET + Odimergie.constants.TEAM + '/' + Odimergie.constants.UPSTREAM + '/pull-requests/',
            'LOGOS': {
                'JIRA': Odimergie.constants.IMAGES.JIRA,
                'BITBUCKET': Odimergie.constants.IMAGES.BITBUCKET,
                'CI': Odimergie.constants.IMAGES.CI,
                'AUTOS': Odimergie.constants.IMAGES.AUTOS,
            },
        },
        tabs: [
            {
                title: 'PR',
                image: Odimergie.constants.IMAGES.BITBUCKET,
                url: Odimergie.constants.BITBUCKET + Odimergie.constants.TEAM + '/' + Odimergie.constants.UPSTREAM + '/pull-requests/',
                enabled: false,
            },
            {
                title: 'Autos Upstream',
                image: Odimergie.constants.IMAGES.AUTOS,
                url: Odimergie.constants[`JENKINS_${jenkinsVersion}`].AUTOS + Odimergie.constants.JOBS.SCHEDULED,
                enabled: false,
            },
            {
                title: 'Jira',
                image: Odimergie.constants.IMAGES.JIRA,
                url: Odimergie.constants.JIRA,
                enabled: false,
            }, {
                title: 'Refresh',
                id: 'odm_refresh_button',
                image: Odimergie.constants.IMAGES.REFRESH,
                enabled: true,
            }, {
                name: 'settings menu',
                id: 'odm_settings_button',
                image: Odimergie.constants.IMAGES.GEAR,
                enabled: true,
            }, {
                title: 'Minimize',
                id: 'odm_resize_button',
                image: Odimergie.constants.IMAGES.MINIMIZE,
                enabled: true,
            },
        ],
        menu: function() {
            let menu = $('<ul></ul>').addClass('odm-hub-header');
            let tabs = this.tabs;
            _.forEach(tabs, function(tab) {
                if (tab.enabled) {
                    menu.append(hub.elementMenu(tab));
                }
            }, this);
            return menu;
        },
        title: function() {
            return $('<span></span>').addClass('odm-primary-color odm-big-text').append(Odimergie.constants.PROJECT_NAME);
        },
        subtitle: function() {
            return $('<span></span>').addClass('odm-primary-color').append('');
        },
        overlay: function() {
            let config = {
                'id': this.id,
                'class': this.id,
            };
            let settings = Odimergie.storage.load();
            return $('<div></div>', config).addClass(settings.HUB.collapsed ? 'odm-rotate-minimize-hub' : '');
        },
        removeHUB: function() {
            let HUB = $('#' + this.id);
            if (HUB) {
                HUB.remove();
            }
        },
        elementMenu: function(tab) {
            return $('<li></li>', {
                id: tab.id ? tab.id : '',
            }).addClass('odm-inline ' + tab.classes ? tab.classes : '')
                .append($('<div></div>').addClass('odm-primary-color odm-reviewer-margin')
                    .append($('<img/>', {
                        src: tab.image,
                    }).attr('alt', tab.name).addClass('odm-reviewer-size')));
        },
        toggleCollapse: function() {
            let getClasses = function(typeCollapse) {
                return {
                    button: 'odm-rotate-' + typeCollapse + '-button',
                    hub: 'odm-rotate-' + typeCollapse + '-hub',
                };
            };
            let settings = Odimergie.storage.load();
            let max = getClasses('maximize');
            let min = getClasses('minimize');
            let selectedClasses = settings.HUB.collapsed ? max : min;
            $('#' + this.id).removeClass(max.hub + ' ' + min.hub).addClass(selectedClasses.hub);
            $(this.locators.resize).removeClass(max.button + ' ' + min.button).addClass(selectedClasses.button);
            settings.HUB.collapsed = !settings.HUB.collapsed;
            Odimergie.storage.save(settings);
        },
        fixVersionToClipboard: function() {
            Odimergie.util.copyToClipboard(Odimergie.Collected.fixVersion);
        },
        toggleOptions: function() {
            Odimergie.settingsMenu.toggleOptions();
        },
        addHUBListeners: function() {
            $(this.locators.resize).on('click', $.proxy(this, 'toggleCollapse'));
            $(this.locators.settings).on('click', $.proxy(this, 'toggleOptions'));
            $('#' + this.locators.fixVersionId).on('click', $.proxy(this, 'fixVersionToClipboard'));
        },
        getIconCopy: function() {
            return '\u270e';
        },
        renderFixVersion: function() {
            return $('<div></div>', {
                id: this.locators.fixVersionId,
            }).append(Odimergie.Collected.fixVersion ? Odimergie.Collected.fixVersion + this.getIconCopy() : '');
        },
        render: function() {
            let that = this;
            if (!Odimergie.Collected.fixVersion) {
                Odimergie.settingsMenu.getSettingsFromServices().then(function() {
                    $('#' + that.locators.fixVersionId).append(Odimergie.Collected.fixVersion + ' ' + that.getIconCopy());
                });
            }
            this.removeHUB();
            $(document.body)
                .append(this.overlay()
                    .append(this.menu())
                    .append(this.renderFixVersion())
                    // .append(this.title())
                    // .append(this.subtitle())
                );
            this.addHUBListeners();
            return $('#' + this.id);
        },
    };
    module.exports = hub;
})();

module.exports = {
    noResponse: {
        odmError: 'Empty Response',
    },
    noExtension: {
        odmError: 'No extension installed',
    },
    noConnection: 'Internal tools doesn\'t response',
};

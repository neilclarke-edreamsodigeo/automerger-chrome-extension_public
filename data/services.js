(function() {
    'use strict';
    const Odimergie = {};
    const $ = require('jquery');
    const _ = require('underscore');
    const Constants = require('../data/constants');
    Odimergie.debugMode = require('../data/debug');
    Odimergie.util = require('../util/util');
    Odimergie.errors = require('../data/errors');
    Odimergie.mentors = require('../data/mentors');
    Odimergie.Collected = Odimergie.Collected || require('../data/collected');
    Odimergie.regExp = require('../data/regExp');


    const services = {
        /**
         *
         *  AJAX METHODS for External Request
         *
         */
        settingsDocsURL: 'https://spreadsheets.google.com/feeds/cells/1lbYhy0a_CO04exk-BkGVAITZqyXSW0nUIgyKHBeJHfQ/od6/public/values?alt=json',
        api: 'http://mad-onefront-02.odigeo.org:50000/api',
        getCIJenkinsURL: function(fork, isUpstream) {
            return 'http://bcn-jenkins-01.odigeo.org/jenkins/job/' + fork + '/' +
                (isUpstream ? 'lastCompletedBuild' : 'lastBuild') +
                '/api/json';
        },
        getAutosJenkinsURL: function(id, type) {
            const portArea = window.localStorage.getItem('areaSelected') || '';
            const isAutosArea = Odimergie.util.isJenkinsType('AREA');
            let autosType = type ? type : (isAutosArea ? 'OneFront-Autos-Main' : 'OneFront-autos-parallel');
            return `${Constants[`JENKINS_${Odimergie.util.getJenkinsVersion()}`].AUTOS}${autosType}/${id}/api/json`.replace('#PORT#', portArea);
        },

        getODFJenkinsUrl: function() {
            // FOR SPECIFIC ID -> http://mad-onefront-02.odigeo.org/jenkins/job/odf/{id}/api/json?pretty=true
            return `${Constants[`JENKINS_${Odimergie.util.getJenkinsVersion()}`].AUTOS}odf/lastCompletedBuild/api/json`;
        },
        getSettingsAutomerger: function(success) {
            if (Odimergie.util.isOneFront()) {
                $.getJSON(this.settingsDocsURL, function(data) {
                    let dataField = data.feed.entry;
                    Odimergie.Collected.mergeEnabled = (dataField[1] && dataField[1].gs$cell.$t.toString() !== 'FALSE');
                    Odimergie.Collected.fixVersion = dataField[3] ? dataField[3].gs$cell.$t.toString() : '';
                    Odimergie.Collected.odiVersion = dataField[5] ? dataField[5].gs$cell.$t.toString() : '';
                    success();
                });
            } else {
                const fork = Odimergie.util.getForkName();

                this.xGet(`${this.api}/${fork}`)
                    .then((data) => {
                        Odimergie.Collected.mergeEnabled = data.mergeEnabled;
                        Odimergie.Collected.fixVersion = data.fixVersion;
                        Odimergie.Collected.odiVersion = data.odiVersion;
                        Odimergie.mentors.setMentors(data.approvals.map((item) => ({userName: item})));
                        success();
                    });
            }
        },
        getUpstreamStatus: function(success) {
            const upstream = Odimergie.util.getUpstreamName();

            this.getCIstatus(upstream, true, success);
        },
        getCIstatus: function(fork, isUpstream, response) {
            if (fork) {
                let url = (fork === 'odf') ? this.getODFJenkinsUrl() : this.getCIJenkinsURL(fork, isUpstream);
                this.crossDomainRequest(url,
                    function(data) {
                        if (data) {
                            if (!data.odmError) {
                                response(data.result === 'SUCCESS' && !data.building ? true : false);
                            } else if (Odimergie.debugMode.request) {
                                data = data || Odimergie.errors.noResponse;
                                Odimergie.debugMode.log('[DEBUG MODE] CI REQUEST: ' + data.odmError);
                                response(false);
                            }
                        } else {
                            Odimergie.debugMode.log('[DEBUG MODE] AUTOS REQUEST: ' + Odimergie.errors.noConnection);
                            response(false);
                        }
                    });
            } else {
                response(false);
            }
        },
        getCILastBuildInfo: function(url, response) {
            this.crossDomainRequest(url,
                function(data) {
                    const buildInfo = {commitId: null, timestamp: data.timestamp, duration: data.duration};
                    if (data.actions) {
                        const mercurialData = data.actions.find((a) => (!!a.mercurialNodeName));
                        if (mercurialData) {
                            buildInfo.commitId = mercurialData.mercurialNodeName;
                        }
                    } else if (data && data.changeSet && data.changeSet.items && data.changeSet.items.length > 0) {
                        buildInfo.commitId = data.changeSet.items[0].commitId;
                    }
                    response(buildInfo);
                });
        },

        getAutosStatus: function(id, response, jobName) {
            this.crossDomainRequest(this.getAutosJenkinsURL(id, jobName),
                function(data) {
                    if (data) {
                        if (!data.odmError) {
                            let formattedData = Odimergie.util.extractAutosData(data);
                            response(formattedData);
                        } else if (Odimergie.debugMode.request) {
                            data = data || Odimergie.errors.noResponse;
                            Odimergie.debugMode.log('[DEBUG MODE] AUTOS REQUEST: ', data.odmError);
                            response(false);
                        }
                    } else {
                        Odimergie.debugMode.log('AUTOS REQUEST: ', Odimergie.errors.noConnection);
                        response(false);
                    }
                });
        },
        getAutosExecutionTime: function(url, response) {
            this.crossDomainRequest(Odimergie.util.getApiDataUrl(url),
                function(data) {
                    response(data ? data.timestamp: data);
                });
        },
        /**
         *
         * @param {String} url
         * @param {Function} response - callback method
         *
         * This is the method for do an External Request with a Chrome Extension
         * and parse to JSON Objects.
         *
         */
        crossDomainRequest: function(url, response) {
            // THIS CONDITIONAL IS CHROME EXTENSION DETECTION
            if (chrome.runtime.onMessage) {
                chrome.runtime.sendMessage({
                    method: 'POST',
                    action: 'xhttp',
                    url: url,
                }, function(responseText) {
                    let formattedData;
                    try {
                        formattedData = JSON.parse(responseText);
                    } catch (error) {
                        formattedData = Odimergie.errors.noResponse;
                    }
                    response(formattedData);
                });
            } else {
                response(Odimergie.errors.noExtension);
            }
        },
        xGet: (url) => {
            return new Promise((resolve, reject) => {
                if (chrome.runtime.onMessage) {
                    chrome.runtime.sendMessage({
                        method: 'GET',
                        action: 'xhttp',
                        url: url,
                    }, (res) => res ? resolve(JSON.parse(res)) : reject());
                } else {
                    reject(Odimergie.errors.noExtension);
                }
            });
        },
        getAutosExecutionIdPlainDescription: function(description, autosIdRegExp) {
            let autosId = [];
            let result;
            while ((result = autosIdRegExp.exec(description)) !== null) {
                autosId.push(result[1]);
            }
            return autosId;
        },
        getIDFromDescription: function(href) {
            let elementSplitter = ['/OneFront-autos-parallel/', '/OneFront-autos-parallel-from-artifact/', '/OneFront-parallel-groups/'];
            let details = {
                id: '',
                jobName: '',
            };
            if (href.indexOf(elementSplitter[0]) > -1) {
                details.id = href ? href.split(elementSplitter[0])[1] : '';
                details.jobName = elementSplitter[0];
            } else if (href.indexOf(elementSplitter[1]) > -1) {
                details.id = href ? href.split(elementSplitter[1])[1] : '';
                details.jobName = elementSplitter[1];
            } else if (href.indexOf(elementSplitter[2]) > -1) {
                details.id = href ? href.split(elementSplitter[2])[1] : '';
                details.jobname = elementSplitter[2];
            }
            details.id = details.id ? details.id.split('%5D')[0] : '';
            return details;
        },
        getIDMainFromDescription: function(href) {
            const elementSplitter = ['/OneFront-Autos-Main/'];
            const details = {
                id: '',
                jobName: '',
            };
            if (href.indexOf(elementSplitter[0]) > -1) {
                details.id = href.split(elementSplitter[0])[1];
                details.jobName = elementSplitter[0];
            } else if (href.indexOf(elementSplitter[1]) > -1) {
                details.id = href.split(elementSplitter[1])[1];
                details.jobName = elementSplitter[1];
            }
            details.id = details.id ? details.id.split('%5D')[0] : '';
            return details;
        },
        getIDSingleFromDescription: function(href) {
            const elementSplitter = ['/OneFront-autos-single/', '/OneFront-Single/'];
            const details = {
                id: '',
                jobName: '',
            };
            if (href.indexOf(elementSplitter[0]) > -1) {
                details.id = href.split(elementSplitter[0])[1];
                details.jobName = elementSplitter[0];
            } else if (href.indexOf(elementSplitter[1]) > -1) {
                details.id = href.split(elementSplitter[1])[1];
                details.jobName = elementSplitter[1];
            }
            details.id = details.id ? details.id.split('%5D')[0] : '';
            return details;
        },
        getAutosParallelIdFromDom: function($description) {
            _.forEach($description, function($autosLink) {
                Odimergie.debugMode.log('', $autosLink);
                let details = services.getIDFromDescription($autosLink.href);
                Odimergie.debugMode.log('ID from Autos Parallels', details.id);
                if (details.id) {
                    services.getAutosStatus(details.id, function(data) {
                        Odimergie.debugMode.log('', data);
                        Odimergie.Collected.Autos.parallels.push(data);
                    }, details.jobName);
                }
            }, this);
        },
        getAutosSingleIdFromDom: function($description) {
            _.forEach($description, function($autosLink) {
                Odimergie.debugMode.log('', $autosLink);
                const details = services.getIDSingleFromDescription($autosLink.href);
                Odimergie.debugMode.log('ID from Autos Parallels', details.id);
                if (details.id) {
                    services.getAutosStatus(details.id, function(data) {
                        Odimergie.debugMode.log('', data);
                        Odimergie.Collected.Autos.singles.push(data);
                    }, details.jobName);
                }
            }, this);
        },
        getAutosMainIdFromDom: function($description) {
            _.forEach($description, function($autosLink) {
                Odimergie.debugMode.log('', $autosLink);
                const details = services.getIDMainFromDescription($autosLink.href);
                Odimergie.debugMode.log('ID from Autos Main', details.id);
                if (details.id) {
                    services.getAutosStatus(details.id, function(data) {
                        Odimergie.debugMode.log('', data);
                        Odimergie.Collected.Autos.main.push(data);
                    }, details.jobName);
                }
            }, this);
        },
    };
    module.exports = services;
})();

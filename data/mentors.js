(function() {
    'use strict';
    const $ = require('jquery');
    const _ = require('underscore');
    const defaultMentors = [
        {
            'userName': 'alperal',
            'project': 'BOOK',
        },
        {
            'userName': 'aitor_calderon',
            'project': 'OF2',
        },
        {
            'userName': 'daniel_garcia_nogales',
            'project': 'OFIS',
        },
        {
            'userName': 'x_garcia',
            'project': 'FC',
        },
        {
            'userName': 'acontell',
            'project': 'DPF',
        },
        {
            'userName': 'miguel-figuerola',
            'project': 'XSELL',
        },
        {
            'userName': 'jgmoran',
            'project': 'SUMO',
        },
        {
            'userName': 'alberto_mateos',
            'project': 'PLUS',
        },
        {
            'userName': 'jose_ramon_perez',
            'project': 'SYRTOF',
        },
        {
            'userName': 'javier-valencia',
            'project': 'ALT',
        },
        {
            'userName': 'davidbeijinhor',
            'project': 'MFS',
        },
        {
            'userName': 'acontell',
            'project': 'UNL',
        },
        {
            'userName': 'javier_mota',
            'project': 'TV',
        },
        {
            'userName': 'javier_mota',
            'project': 'FF',
        },
        {
            'userName': 'javier_mota',
            'project': 'DPM',
        },
        {
            'userName': 'brujula_jmartorell',
            'project': 'PC',
        },
        {
            'userName': 'brujula_jmartorell',
            'project': 'TMC',
        },
        {
            'userName': 'javier-valencia',
            'project': 'PNF',
        },
    ];

    module.exports = {
        mentors: [],
        confluenceStatus: false,
        mentorsLink: 'https://jira.odigeo.com/wiki/display/MOBILEWEB/OF+Mentors',
        mentorsTableIdCol: 'ID',
        mentorsTableProjectCol: 'Project',
        mentorsPromise: null,
        fetchMentorsTable: function() {
            let that = this;
            $.ajax({
                type: 'GET',
                url: this.mentorsLink,
                success: function(data) {
                    that.checkData(data);
                },
                error: function() {
                    that.setMentors(defaultMentors);
                },
            });
        },
        checkData: function(data) {
            let matches = data.match(/<tbody(.*?)<\/tbody>/);
            if (_.isNull(matches)) {
                return this.setMentors(defaultMentors, false);
            }
            return this.parseData(matches);
        },
        parseData: function(matches) {
            let that = this;
            let table = $.parseHTML(matches[0])[0];
            let idProj = [0, 0];
            let thList = table.querySelectorAll('tr > th');
            let tdList = table.querySelectorAll('tr > td');
            let nCol = thList.length;
            let mentorsList = [];

            _.each(thList, function(th, idx) {
                if (th.textContent.indexOf(that.mentorsTableIdCol) !== -1) {
                    idProj[0] = nCol-idx-1;
                } else if (th.textContent.indexOf(that.mentorsTableProjectCol) !== -1) {
                    idProj[1] = nCol-idx-1;
                }
            });
            for (let it=nCol-1; it<tdList.length; it+=nCol) {
                mentorsList.push({
                    userName: tdList[it-idProj[0]].textContent,
                    project: tdList[it-idProj[1]].textContent,
                });
            }
            this.setMentors(mentorsList, true);
        },
        setMentors: function(mentorsList, status=this.confluenceStatus) {
            if (mentorsList.length > 0) {
                _.extend(this.mentors, mentorsList);
            }
            this.setStatus(status);
            if (this.mentorsPromise !== null) {
                this.mentorsPromise();
            }
        },
        setStatus: function(status) {
            if (this.confluenceStatus !== status) {
                this.confluenceStatus = !this.confluenceStatus;
            }
        },
        getMentors: function() {
            return this.mentors;
        },
        getStatus: function() {
            return this.confluenceStatus;
        },
        getMentorsPromise: function() {
            let that = this;
            return new Promise(function(resolve) {
                that.mentorsPromise = resolve;
                if (!_.isEmpty(that.mentors)) {
                    that.mentorsPromise();
                }
            });
        },
    };
})();

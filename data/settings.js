/**
 * Default values for the user script
 */
module.exports = {
    // storage version only
    version: '0.1',
    tools: {},
    HUB: {
        style: {},
        collapsed: false,
    },
};

const images = {
    'JIRA': require('../images/jira.png'),
    'BITBUCKET': require('../images/bitbucket.png'),
    'CI': require('../images/jenkins.png'),
    'GEAR': require('../images/gear.png'),
    'MINIMIZE': require('../images/minimize.png'),
    'REFRESH': require('../images/refresh.png'),
};

images.AUTOS = images.CI;

module.exports = images;

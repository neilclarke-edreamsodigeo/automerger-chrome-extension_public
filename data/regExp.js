(function() {
    'use strict';
    module.exports = {
        ticketPattern: /((?:FC|PC|GT|TV|PD|[A-Z]{3,})-\d{1,4})/g,
        autosId: /-autos-parallel\/(\d*)/g,
        autosSingleId: /-autos-single\/(\d*)/g,
        forkNameDescription: /\/(frontend-(html5|home)_[\w,-]*)/,
        page: {
            pullRequestList: /^https:\/\/bitbucket.org\/[\w-]*\/[\w-]*\/pull-requests\/$/,
            pullRequest: /^https:\/\/bitbucket.org\/[\w-]*\/\S+\/pull-requests\/\d+/,
            createPullRequest: /^https:\/\/bitbucket.org\/[\w-]*\/\S+\/pull-requests\/new/,
            CI: /^$/,
            Autos: /^$/,
            Stabilization: /^$/,
            Ticket: /^$/,
        },
        forks: {
            'upstream': /\/frontend-(html5|home)\//,
            'prod': /\/frontend-(html5|home)_prod-/,
            'private': /\/frontend-(html5|home)_/,
            'odf': '/odf',
        },
        forkRegex: /odigeoteam\/(frontend-(?:html5|home))/,
        extractTicketCodeList: function(text) {
            let ticketList = [];
            let match;
            do {
                match = this.ticketPattern.exec(text);
                if (match) {
                    ticketList.push(match[1]);
                }
            } while (match);
            return ticketList;
        },
    };
})();

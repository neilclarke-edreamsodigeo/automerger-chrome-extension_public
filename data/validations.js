(function() {
    'use strict';
    const Odimergie = {};
    const _ = require('underscore');
    const {isOneFront} = require('../util/util');
    Odimergie.constants = require('../data/constants');
    Odimergie.rules = require('../data/rules');
    Odimergie.mentors = require('../data/mentors');
    isOneFront() && Odimergie.mentors.fetchMentorsTable();
    Odimergie.Collected = Odimergie.Collected || require('../data/collected');
    module.exports = {
        'MIN_REVIEWERS': {
            id: 'MIN_REVIEWERS',
            title: Odimergie.constants.CMS.VALIDATIONS.MIN_REVIEWERS,
            blocker: true,
            enable: {
                upstream: true,
                private: true,
                prod: true,
                odf: true,
            },
            execute: function(data) {
                let collection = data || Odimergie.Collected;
                return collection.approves.quantity >= Odimergie.rules.MIN_REVIEWERS_NEEDED;
            },
        },
        'ALL_REVIEWERS': {
            id: 'ALL_REVIEWERS',
            title: Odimergie.constants.CMS.VALIDATIONS.ALL_REVIEWERS,
            blocker: true,
            enable: {
                upstream: true,
                private: true,
                prod: true,
                odf: true,
            },
            execute: function(data) {
                let collection = data || Odimergie.Collected;
                return collection.reviewers.length === collection.approves.quantity;
            },
        },
        'LOGIN_CONFLUENCE': {
            id: 'LOGIN_CONFLUENCE',
            title: Odimergie.constants.CMS.VALIDATIONS.LOGIN_CONFLUENCE,
            blocker: false,
            enable: {
                upstream: true,
                private: false,
                prod: true,
            },
            execute: function() {
                if (!Odimergie.mentors.getStatus()) {
                    return false;
                }
                this.enable = false;
                return true;
            },
        },
        'ONE_MENTOR': {
            id: 'ONE_MENTOR',
            title: Odimergie.constants.CMS.VALIDATIONS.ONE_MENTOR,
            blocker: true,
            enable: {
                upstream: !isOneFront(),
                private: false,
                prod: !isOneFront(),
            },
            execute: function(data) {
                let collection = data || Odimergie.Collected;
                if (collection.owner.isMentor) {
                    return true;
                }
                let dataApproves = collection.approves;
                let mentorApprove = false;
                _.forEach(dataApproves.who, function(user) {
                    _.forEach(Odimergie.mentors.getMentors(), function(mentor) {
                        mentorApprove = !mentorApprove ? user === mentor.userName : mentorApprove;
                    }, user);
                });
                return mentorApprove;
            },
        },
        'YOUR_MENTOR': {
            id: 'YOUR_MENTOR',
            title: Odimergie.constants.CMS.VALIDATIONS.YOUR_MENTOR,
            blocker: true,
            enable: false,
            execute: function(data) {
                let collection = data || Odimergie.Collected;
                if (collection.owner.isMentor || !collection.owner.hasMentor) {
                    return true;
                }
                let dataApproves = collection.approves;
                let mentorApprove = false;
                let project = collection.project;
                _.forEach(dataApproves.who, function(reviewer) {
                    _.forEach(Odimergie.mentors.getMentors(), function(mentor) {
                        if (reviewer === mentor.userName) {
                            if (project === mentor.project) {
                                mentorApprove = true;
                            }
                        }
                    }, reviewer);
                });
                return mentorApprove;
            },
        },
        'CI_PASSED': {
            id: 'CI_PASSED',
            title: Odimergie.constants.CMS.VALIDATIONS.CI_PASSED,
            enable: {
                upstream: true,
                private: false,
                prod: true,
                odf: true,
            },
            blocker: true,
            execute: function(data) {
                let collection = data || Odimergie.Collected;
                return collection.CI.status;
            },
        },
        'NO_CONFLICTS': {
            id: 'NO_CONFLICTS',
            title: Odimergie.constants.CMS.VALIDATIONS.NO_CONFLICTS,
            blocker: true,
            enable: {
                upstream: true,
                private: true,
                prod: true,
                odf: true,
            },
            execute: function(data) {
                let collection = data || Odimergie.Collected;
                return !collection.conflicts;
            },
        },
        'AUTOS_PASSED': {
            id: 'AUTOS_PASSED',
            title: Odimergie.constants.CMS.VALIDATIONS.AUTOS_PASSED,
            blocker: false,
            enable: {
                upstream: true,
                private: false,
                prod: true,
            },
            execute: function(data) {
                let rule = true;
                let collection = data || Odimergie.Collected;
                _.forEach(collection.Autos.main, function(main) {
                    if (!main.status) {
                        rule = false;
                    }
                });
                return collection.Autos.main.length > 0 ? rule : false;
            },
        },
        'AUTOS_FORK': {
            id: 'AUTOS_FORK',
            title: Odimergie.constants.CMS.VALIDATIONS.AUTOS_FORK,
            blocker: true,
            enable: {
                upstream: true,
                private: false,
                prod: true,
            },
            execute: function(data) {
                const collection = data || Odimergie.Collected;
                const autos = collection.Autos.parallels.concat(collection.Autos.singles).concat(collection.Autos.main);
                let rule = true;
                _.forEach(autos, (execution) => {
                    const {forkName: executionForkName = execution['FORK_NAME']} = execution;
                    const {forkName: collectionForkName} = collection;
                    if (collectionForkName && executionForkName) {
                        if (!collectionForkName.includes(executionForkName) && executionForkName !== Odimergie.constants.UPSTREAM_FORK) {
                            rule = false;
                        }
                    }
                });
                return autos.length > 0 ? rule : false;
            },
        },
        'AUTOS_EXPIRED': {
            id: 'AUTOS_EXPIRED',
            title: Odimergie.constants.CMS.VALIDATIONS.AUTOS_EXPIRED,
            blocker: true,
            enable: {
                upstream: true,
                private: false,
                prod: true,
            },
            execute: function(data) {
                let collection = data || Odimergie.Collected;
                const autos = collection.Autos.parallels.concat(collection.Autos.singles).concat(collection.Autos.main);
                let rule = !_.some(autos, function(execution) {
                    return execution.lastUpdate > 5;
                });
                return autos.length > 0 ? rule : false;
            },
        },
        'AUTOS_DESKTOP': {
            id: 'AUTOS_DESKTOP',
            title: Odimergie.constants.CMS.VALIDATIONS.AUTOS_DESKTOP,
            blocker: false,
            enable: {
                upstream: false,
                private: false,
                prod: false,
            },
            execute: function(data) {
                let rule = false;
                let collection = data || Odimergie.Collected;
                _.forEach(collection.Autos.parallels, function(parallel) {
                    if (parallel.browser === 'chrome') {
                        rule = true;
                    }
                });
                return collection.Autos.parallels.length > 0 ? rule : false;
            },
        },
        'AUTOS_MOBILE': {
            id: 'AUTOS_MOBILE',
            title: Odimergie.constants.CMS.VALIDATIONS.AUTOS_MOBILE,
            blocker: false,
            enable: {
                upstream: false,
                private: false,
                prod: false,
            },
            execute: function(data) {
                let rule = false;
                let collection = data || Odimergie.Collected;
                _.forEach(collection.Autos.parallels, function(parallel) {
                    if (parallel.browser === 'chromeMobile') {
                        rule = true;
                    }
                });
                return collection.Autos.parallels.length > 0 ? rule : false;
            },
        },
        'AUTOS_MAIN': {
            id: 'AUTOS_MAIN',
            title: Odimergie.constants.CMS.VALIDATIONS.AUTOS_MAIN,
            blocker: false,
            enable: {
                upstream: true,
                private: false,
                prod: true,
            },
            execute: function(data) {
                let collection = data || Odimergie.Collected;
                return collection.Autos.main.length > 0;
            },
        },
        'CI_SYNCHRO': {
            id: 'CI_SYNCHRO',
            title: Odimergie.constants.CMS.VALIDATIONS.CI_SYNCHRO,
            blocker: true,
            enable: {
                upstream: true,
                private: false,
                prod: false,
            },
            execute: function(data) {
                let collection = data || Odimergie.Collected;
                return collection.isCiSynchro;
            },
        },
        'CI_AUTOS_SYNCHRO': {
            id: 'CI_AUTOS_SYNCHRO',
            title: Odimergie.constants.CMS.VALIDATIONS.CI_AUTOS_SYNCHRO,
            blocker: false,
            enable: {
                upstream: true,
                private: false,
                prod: false,
            },
            execute: function(data) {
                let collection = data || Odimergie.Collected;
                return collection.isCiAutosSynchro;
            },
        },
    };
})();

module.exports = {
    id: '',
    title: '',
    description: '',
    owner: {
        name: '',
        isMentor: false,
        hasMentor: false,
    },
    reviewers: [],
    project: '',
    tickets: [],
    isUpstream: false,
    isPrivate: false,
    readyForMerge: false,
    mergeEnabled: false,
    upstreamCIstatus: false,
    isCiAutosSynchro: false,
    isCiSynchro: false,
    CI: {
        status: false,
    },
    Autos: {
        /**
         * @params
         * status
         * lastUpdate
         * forkName
         */
        main: [],
        parallels: [],
        singles: [],
    },
    forkName: '',
    dataValidation: {
        passed: 0,
        failed: 0,
        warning: 0,
    },
    comments: {
        quantity: 0,
        authors: [],
    },
    approves: {
        quantity: 0,
        who: [],
    },
    conflicts: false,
};

# Automerger

[![Install on Chrome Store](https://developer.chrome.com/webstore/images/ChromeWebStore_Badge_v2_206x58.png)](https://chrome.google.com/webstore/detail/automerger/egobkcbcfgokdgmlfchgilchnecnhhma)

Check the [Confluence page](https://jira.odigeo.com/wiki/display/MOBILEWEB/Automerger).

[Jenkins Job](http://mad-onefront-02.odigeo.org:48005/job/automerger-chrome-extension/job/master/)

(function() {
    'use strict';
    const regularExp = require('./data/regExp');
    const router = require('./util/router');

    let currentPage = '';
    const setCurrentPage = function() {
        let currentUrl = window.location.href;
        let page;
        for (page in regularExp.page) {
            if (regularExp.page[page].exec(currentUrl + '')) {
                currentPage = page;
                break;
            }
        }
    };
    const init = function() {
        setCurrentPage();
    };

    init();
    if (currentPage) {
        router[currentPage].render();
    }
})();
